﻿Option Explicit On
Imports System.ComponentModel

Module modUltra
    Public Function UltraDownload(ByVal request As String) As String
        Dim testDownload As New net.ultraasp.client.download.Service
        Return testDownload.DownloadDataByUserString("unclebuck", "Int_CA", "Unc13Buck", request)
    End Function

    Public Function GetUltraDownload(ByVal request As String, ByVal un As String, ByVal pw As String) As String
        Dim thisDownload As New net.ultraasp.client.download.Service
        Return thisDownload.DownloadDataByUserString("unclebuck", un, pw, request)
    End Function

    Public Function GetUltraUpload(ByVal request As String, ByVal un As String, ByVal pw As String) As String
        Dim thisDownload As New net.ultraasp.client.upload.Service
        Return thisDownload.UploadDataByUserString("unclebuck", un, pw, request)
    End Function

    'Public Function UltraUpload(ByVal request As String, ByVal un As String, ByVal pw As String) As String
    '    Dim upload As New net.ultraasp.client.upload.Service
    '    Return upload.UploadDataByUserString("unclebuck", un, pw, request)
    'End Function

    Public Sub updateDialler(ByVal worker As BackgroundWorker, ByVal e As DoWorkEventArgs)
        'Try
        If worker.CancellationPending Then
                e.Cancel = True
                MsgBox("Upload Thread Complete. You can exit now or restart the update.")
            Else
                'Try
                Call ConnectSqlSendSOAPs("exec DiallerStagingArea.dbo.sp_UpdateSDB")
                'Catch
                'End Try

                'Dim runNow As String = ConnectToSql("DiallerStagingArea.dbo.sp_AccountsToCompare_CheckToRun ", False)
                'If runNow.ToString = "1" Then
                '    sqlToDataTable("insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Account State to be Checked' ")
                '    Call CheckAccountStates()
                'End If
            End If
        'Catch
        'End Try
    End Sub

    Sub DispositionsDownloadByID()
        Try
            Try
                Dim query As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Dispositions start from integration' "
                Call ConnectToSql(query, False)
            Catch
            End Try

            Dim id As String = ConnectToSql("exec DiallerStagingArea.[dbo].[sp_CallAttempts_LastDispositionID] ", False)
            Dim request = "<data clientid='10864'><Report.Disposition.CDR RowCount='950' ID='" & id.ToString & "' /></data>"
            Dim returnVal = GetUltraDownload(request, "Int_CA", "Unc13Buck")

            If getRowNumbers(returnVal) > 0 Then
                Try
                    Dim query = "exec DiallerStagingArea.[dbo].[sp_DownloadDataStoreXML] 'Dispositions' , '" & returnVal & "' "
                    Call ConnectToSql(query, False)
                Catch
                End Try
                'Else
                '    Threading.Thread.Sleep(1000 * 60 * 10)
            End If

            Try
                Dim query As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Dispositions end @ integration' "
                Call ConnectToSql(query, False)
            Catch
            End Try
        Catch
        End Try
    End Sub

    Sub CallAttemptsDownloadByID()
        Try
            Try
                'Try
                '    'Call Form1.updateListBox(Now.ToString & " -> Starting Call Attempts download", "lsbDownloadProgress")
                'Catch e As Exception
                '    MsgBox(e)
                'End Try

                Dim query As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update CallAttempts start from integration' "
                Call ConnectToSql(query, False)
            Catch
            End Try

            'Try
            '    'Call Form1.updateListBox(Now.ToString & " -> Getting last Call Attempts ID", "lsbDownloadProgress")
            'Catch e As Exception
            '    MsgBox(e)
            'End Try

            Dim id As String = ConnectToSql("exec DiallerStagingArea.[dbo].[sp_CallAttempts_LastCDR_ID] ", False)

            'Try
            '    'Call Form1.updateListBox(Now.ToString & " -> Getting Call Attempts from Ultra", "lsbDownloadProgress")
            'Catch e As Exception
            '    MsgBox(e)
            'End Try

            Dim request = "<data clientid='10864'><Report.CustomerCDRId ID='" & id.ToString & "' RowCount='950' /></data>"
            Dim returnVal = GetUltraDownload(request, "Int_CA", "Unc13Buck")
            'MsgBox(returnVal)

            If getRowNumbers(returnVal) > 0 Then
                'MsgBox("rows to download")
                Try
                    'MsgBox("send ca to laps")
                    Dim query As String = "exec DiallerStagingArea.[dbo].[sp_DownloadDataStoreXML] 'CDR' , '" & returnVal & "' "
                    'Dim result As String = ConnectToSql(query, False)
                    'MsgBox(result & " ca sent to laps")
                    Call ConnectToSql(query, False)
                    'MsgBox("ca sent to laps")
                Catch
                End Try

                Try
                    'MsgBox("getting lists")
                    Call getLists()
                    'MsgBox("got lists")
                Catch
                End Try

                Threading.Thread.Sleep(1000 * 60 * 5)
            Else
                'MsgBox("0 rows to download")
                Threading.Thread.Sleep(1000 * 60 * 15)
            End If
            'https://social.msdn.microsoft.com/Forums/vstudio/en-US/dafcfdde-b238-410b-8c82-4f4afcb9aee5/vs2015-error-8007006e-unable-to-finish-updating-resource-for-binreleaseapppublishsetupexe?forum=visualstudiogeneral

            Try
                Dim query As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update CallAttempts end @ integration' "
                Call ConnectToSql(query, False)
            Catch
            End Try
        Catch
        End Try
    End Sub

    Sub CallAttemptsDownloadByDate()
        Try
            Dim query As String = "exec DiallerStagingArea.dbo.sp_CallAttempts_GetLastStart "
            Dim startDate As DateTime = New DateTime()
            startDate = GetDateTime(ConnectToSql(query, False))
            Dim endDate As Date = DateAdd(DateInterval.Hour, 9, startDate)

            Dim request As String = "<data clientid='10864'><Report.CustomerCDR Start='" & startDate & "' End='" & endDate & "' RowCount='500' /></data>"
            Dim returnVal As String = UltraDownload(request)
            query = "exec DiallerStagingArea.[dbo].[sp_DownloadDataStoreXML] 'CDR' , '" & returnVal & "' "
            Call ConnectToSql(query, False)
        Catch
        End Try
    End Sub

    Public Function buildEnvelope(ByVal xmlString As String, ByVal schema As String, ByVal baseIndex As String) As String
        Dim sEnv As String = "<?xml version=""1.0"" encoding=""utf-8""?>"
        sEnv = sEnv & "<soap12:Envelope xmlns:xsi=""http://www.w3.org/2001/XMLSchema-instance"" xmlns:xsd=""http://www.w3.org/2001/XMLSchema"" xmlns:soap12=""http://www.w3.org/2003/05/soap-envelope"">"
        sEnv = sEnv & "<soap12:Body>"
        sEnv = sEnv & "<UploadDataByUser xmlns=""http://ultraasp.net/"">"
        sEnv = sEnv & "<domain>unclebuck</domain>"
        sEnv = sEnv & "<username>UploaderSQL</username>"
        sEnv = sEnv & "<password>Password1</password>"
        sEnv = sEnv & "<xmlData>"
        sEnv = sEnv & "<upload clientid=""10864"">"
        sEnv = sEnv & "<" & schema & " BaseIndexId=""" & baseIndex & """>"
        sEnv = sEnv & "<rows>"
        sEnv = sEnv & xmlString
        sEnv = sEnv & "</rows>"
        sEnv = sEnv & "</" & schema & ">"
        sEnv = sEnv & "</upload>"
        sEnv = sEnv & "</xmlData>"
        sEnv = sEnv & "</UploadDataByUser>"
        sEnv = sEnv & "</soap12:Body>"
        sEnv = sEnv & "</soap12:Envelope>"

        Return sEnv
    End Function

    Public Function sendSoap(ByVal url As String, ByVal envelope As String, Optional ByVal listID As Integer = 0) As String
        ' Set and Instantiate the working objects
        Dim ObjHTTP As Object = CreateObject("MSXML2.ServerXMLHTTP")

        ' Invoke the web service
        With ObjHTTP
            .open("POST", url, False)
            .setRequestHeader("Content-Type", "application/soap+xml; charset=utf-8")
            .setTimeouts(60000, 60000, 1200000, 1200000)
            .send(envelope)

            Dim myString As String = .responseText
            Dim status As Integer = .status.ToString()

            If status = 200 Then
                Return myString
            Else
                Return "Status code: " & status
            End If
        End With
        ObjHTTP = Nothing
    End Function

    Public Function sendJson(ByVal url As String, ByVal envelope As String, Optional ByVal listID As Integer = 0) As String
        ' Set and Instantiate the working objects
        Dim ObjHTTP As Object = CreateObject("MSXML2.ServerXMLHTTP")

        ' Invoke the web service
        With ObjHTTP
            .open("POST", url, False)
            .setRequestHeader("Content-Type", "application/json; charset=utf-8")
            .setTimeouts(60000, 60000, 1200000, 1200000)
            .send(envelope)

            Dim myString As String = .responseText
            Dim status As Integer = .status.ToString()

            If status = 200 Then
                Return myString
            Else
                Return "Status code: " & status
            End If
        End With
        ObjHTTP = Nothing
    End Function
End Module
