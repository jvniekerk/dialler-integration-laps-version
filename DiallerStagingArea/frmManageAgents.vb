﻿Public Class frmManageAgents
    Inherits System.Windows.Forms.Form

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        Form1.Show()
        Close()
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Dim query As String
        Dim userID As Integer = txtUserID.Text
        Dim listID As Integer = txtListID.Text
        Dim username As String = txtUserName.Text
        Dim induction As Integer = 0
        Dim success As String = "0"

        If rdbInduction.Checked = True Then induction = 1

        If rdbAddAgent.Checked = True Then
            query = "exec DiallerStagingArea.dbo.sp_CollectionsAgents_AddUser @userID = " & userID.ToString & ", @listID = " _
                & listID.ToString & ", @userName = '" & username.ToString & "', @induction = " & induction.ToString
            success = ConnectToSql(query, False)
        Else
            query = "exec DiallerStagingArea.dbo.sp_CollectionsAgents_DeleteUser @userID = " & userID.ToString & ", @listID = " & listID.ToString
            If MsgBox("Are you sure you want to delete the Agent from the database") = 1 Then success = ConnectToSql(query, False)
        End If

        If success = "0" Then
            MsgBox("You have successfully updated the Agent record.")
        Else
            MsgBox("There was a problem with updating the Agent record. Please try again.")
        End If
    End Sub

    Private Sub rdbDeleteAgent_CheckedChanged(sender As Object, e As EventArgs) Handles rdbDeleteAgent.CheckedChanged
        If rdbDeleteAgent.Checked = True Then
            rdbAddAgent.Checked = False
            txtUserName.Visible = False
            lblUserName.Visible = False
            rdbInduction.Visible = False
            rdbPermanent.Visible = False
        Else
            rdbAddAgent.Checked = True
        End If
    End Sub

    Private Sub rdbAddAgent_CheckedChanged(sender As Object, e As EventArgs) Handles rdbAddAgent.CheckedChanged
        If rdbAddAgent.Checked = True Then
            rdbDeleteAgent.Checked = False
            txtUserName.Visible = True
            lblUserName.Visible = True
            rdbInduction.Visible = True
            rdbPermanent.Visible = True
        Else
            rdbDeleteAgent.Checked = True
        End If
    End Sub

    Private Sub rdbPermanent_CheckedChanged(sender As Object, e As EventArgs) Handles rdbPermanent.CheckedChanged
        If rdbInduction.Checked = True Then
            rdbPermanent.Checked = False
        Else
            rdbPermanent.Checked = True
        End If
    End Sub

    Private Sub rdbInduction_CheckedChanged(sender As Object, e As EventArgs) Handles rdbInduction.CheckedChanged
        If rdbPermanent.Checked = True Then
            rdbInduction.Checked = False
        Else
            rdbInduction.Checked = True
        End If
    End Sub
End Class