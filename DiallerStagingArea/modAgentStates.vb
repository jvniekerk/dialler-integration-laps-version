﻿Module modAgentStates
    Sub UpdateAgentStates()
        Try
            Try
                Dim sql As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Agent States start in integration' "
                Call ConnectToSql(sql, False)
            Catch
            End Try

            Dim un As String
            Dim pw As String
            Dim schema As String = "StagingAreaIntegration"
            Dim request As String
            Dim response As String
            Dim query As String

            un = "Int_AgentState"
            pw = "Unc13Buck"

            query = "exec DiallerStagingArea.dbo.sp_GetLastAgentStateID"
            Dim id As String = ConnectToSql(query, False)

            request = "<data clientid='10864'><Report.Agent.Status.CDRId RowCount='500' Id='" & id.ToString & "' /></data>"
            response = GetUltraDownload(request, un, pw)

            If getRowNumbers(response) > 0 Then
                Try
                    query = "exec DiallerStagingArea.dbo.sp_UpdateAgentState @xml = '" & response & "' "
                    Call ConnectToSql(query, False)
                Catch
                End Try

                Try
                    Call GetUserStates()
                Catch
                End Try

                Try
                    Call UpdateUsers()
                Catch
                End Try
                'Else
                '    Threading.Thread.Sleep(1000 * 60 * 10)
            End If

            Try
                Dim sql As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Agent States end @ integration' "
                Call ConnectToSql(sql, False)
            Catch
            End Try
        Catch
        End Try
    End Sub

    Sub GetUserStates()
        Try
            Try
                Dim sql As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Get User States start in integration' "
                Call ConnectToSql(sql, False)
            Catch
            End Try

            Dim un As String
            Dim pw As String
            Dim request As String
            Dim response As String
            Dim query As String

            un = "Int_AgentState"
            pw = "Unc13Buck"

            request = "<data clientid='10864'><Client.Users.State.States /></data>"
            response = GetUltraDownload(request, un, pw)
            query = "exec DiallerStagingArea.dbo.sp_Update_UserStates @xml = '" & response & "' "
            Call ConnectToSql(query, False)

            Try
                Dim sql As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Get User States end @ integration' "
                Call ConnectToSql(sql, False)
            Catch
            End Try
        Catch
        End Try
    End Sub

    Sub UpdateUsers()
        Try
            Try
                Dim sql As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Users start in integration' "
                Call ConnectToSql(sql, False)
            Catch
            End Try

            Dim un As String
            Dim pw As String
            Dim request As String
            Dim response As String
            Dim query As String

            un = "Int_AgentState"
            pw = "Unc13Buck"

            request = "<data clientid='10864'><Framework.User /></data>"
            response = GetUltraDownload(request, un, pw)
            'MsgBox(response)
            'Call saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\test.txt", response)
            query = "exec DiallerStagingArea.dbo.sp_Update_Users @xml = '" & response & "' "
            Call ConnectToSql(query, False)

            Try
                Dim sql As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Users end @ integration' "
                Call ConnectToSql(sql, False)
            Catch
            End Try
        Catch
        End Try
    End Sub
End Module
