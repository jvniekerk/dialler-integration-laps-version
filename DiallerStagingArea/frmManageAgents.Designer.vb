﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class frmManageAgents
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.rdbAddAgent = New System.Windows.Forms.RadioButton()
        Me.rdbDeleteAgent = New System.Windows.Forms.RadioButton()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.txtUserID = New System.Windows.Forms.TextBox()
        Me.txtListID = New System.Windows.Forms.TextBox()
        Me.txtUserName = New System.Windows.Forms.TextBox()
        Me.rdbPermanent = New System.Windows.Forms.RadioButton()
        Me.rdbInduction = New System.Windows.Forms.RadioButton()
        Me.lblUserName = New System.Windows.Forms.Label()
        Me.lblListID = New System.Windows.Forms.Label()
        Me.lblUserID = New System.Windows.Forms.Label()
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'rdbAddAgent
        '
        Me.rdbAddAgent.AutoSize = True
        Me.rdbAddAgent.Checked = True
        Me.rdbAddAgent.Location = New System.Drawing.Point(44, 56)
        Me.rdbAddAgent.Name = "rdbAddAgent"
        Me.rdbAddAgent.Size = New System.Drawing.Size(161, 37)
        Me.rdbAddAgent.TabIndex = 0
        Me.rdbAddAgent.TabStop = True
        Me.rdbAddAgent.Text = "Add Agent"
        Me.rdbAddAgent.UseVisualStyleBackColor = True
        '
        'rdbDeleteAgent
        '
        Me.rdbDeleteAgent.AutoSize = True
        Me.rdbDeleteAgent.Location = New System.Drawing.Point(254, 56)
        Me.rdbDeleteAgent.Name = "rdbDeleteAgent"
        Me.rdbDeleteAgent.Size = New System.Drawing.Size(188, 37)
        Me.rdbDeleteAgent.TabIndex = 1
        Me.rdbDeleteAgent.Text = "Delete Agent"
        Me.rdbDeleteAgent.UseVisualStyleBackColor = True
        '
        'btnExit
        '
        Me.btnExit.Location = New System.Drawing.Point(44, 338)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(251, 69)
        Me.btnExit.TabIndex = 2
        Me.btnExit.Text = "Return to Main UI"
        Me.btnExit.UseVisualStyleBackColor = True
        '
        'btnRun
        '
        Me.btnRun.Location = New System.Drawing.Point(329, 338)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(251, 69)
        Me.btnRun.TabIndex = 3
        Me.btnRun.Text = "Run"
        Me.btnRun.UseVisualStyleBackColor = True
        '
        'Panel1
        '
        Me.Panel1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
        Me.Panel1.Controls.Add(Me.txtUserID)
        Me.Panel1.Controls.Add(Me.txtListID)
        Me.Panel1.Controls.Add(Me.txtUserName)
        Me.Panel1.Controls.Add(Me.rdbPermanent)
        Me.Panel1.Controls.Add(Me.rdbInduction)
        Me.Panel1.Controls.Add(Me.lblUserName)
        Me.Panel1.Controls.Add(Me.lblListID)
        Me.Panel1.Controls.Add(Me.lblUserID)
        Me.Panel1.Location = New System.Drawing.Point(34, 126)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(546, 179)
        Me.Panel1.TabIndex = 4
        '
        'txtUserID
        '
        Me.txtUserID.Location = New System.Drawing.Point(151, 11)
        Me.txtUserID.Name = "txtUserID"
        Me.txtUserID.Size = New System.Drawing.Size(326, 40)
        Me.txtUserID.TabIndex = 7
        Me.txtUserID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtListID
        '
        Me.txtListID.Location = New System.Drawing.Point(151, 44)
        Me.txtListID.Name = "txtListID"
        Me.txtListID.Size = New System.Drawing.Size(326, 40)
        Me.txtListID.TabIndex = 8
        Me.txtListID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtUserName
        '
        Me.txtUserName.Location = New System.Drawing.Point(151, 83)
        Me.txtUserName.Name = "txtUserName"
        Me.txtUserName.Size = New System.Drawing.Size(326, 40)
        Me.txtUserName.TabIndex = 9
        Me.txtUserName.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'rdbPermanent
        '
        Me.rdbPermanent.AutoSize = True
        Me.rdbPermanent.Location = New System.Drawing.Point(237, 120)
        Me.rdbPermanent.Name = "rdbPermanent"
        Me.rdbPermanent.Size = New System.Drawing.Size(240, 37)
        Me.rdbPermanent.TabIndex = 4
        Me.rdbPermanent.Text = "Permanent Group"
        Me.rdbPermanent.UseVisualStyleBackColor = True
        '
        'rdbInduction
        '
        Me.rdbInduction.AutoSize = True
        Me.rdbInduction.Checked = True
        Me.rdbInduction.Location = New System.Drawing.Point(8, 120)
        Me.rdbInduction.Name = "rdbInduction"
        Me.rdbInduction.Size = New System.Drawing.Size(223, 37)
        Me.rdbInduction.TabIndex = 3
        Me.rdbInduction.TabStop = True
        Me.rdbInduction.Text = "Induction Group"
        Me.rdbInduction.UseVisualStyleBackColor = True
        '
        'lblUserName
        '
        Me.lblUserName.AutoSize = True
        Me.lblUserName.Location = New System.Drawing.Point(8, 84)
        Me.lblUserName.Name = "lblUserName"
        Me.lblUserName.Size = New System.Drawing.Size(136, 33)
        Me.lblUserName.TabIndex = 2
        Me.lblUserName.Text = "User Name"
        '
        'lblListID
        '
        Me.lblListID.AutoSize = True
        Me.lblListID.Location = New System.Drawing.Point(8, 51)
        Me.lblListID.Name = "lblListID"
        Me.lblListID.Size = New System.Drawing.Size(82, 33)
        Me.lblListID.TabIndex = 1
        Me.lblListID.Text = "List ID"
        '
        'lblUserID
        '
        Me.lblUserID.AutoSize = True
        Me.lblUserID.Location = New System.Drawing.Point(8, 18)
        Me.lblUserID.Name = "lblUserID"
        Me.lblUserID.Size = New System.Drawing.Size(95, 33)
        Me.lblUserID.TabIndex = 0
        Me.lblUserID.Text = "User ID"
        '
        'frmManageAgents
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(14.0!, 33.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(622, 449)
        Me.Controls.Add(Me.Panel1)
        Me.Controls.Add(Me.btnRun)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.rdbDeleteAgent)
        Me.Controls.Add(Me.rdbAddAgent)
        Me.Font = New System.Drawing.Font("Calibri", 10.125!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(3, 4, 3, 4)
        Me.Name = "frmManageAgents"
        Me.Text = "Manage Agents"
        Me.Panel1.ResumeLayout(False)
        Me.Panel1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents rdbAddAgent As RadioButton
    Friend WithEvents rdbDeleteAgent As RadioButton
    Friend WithEvents btnExit As Button
    Friend WithEvents btnRun As Button
    Friend WithEvents Panel1 As Panel
    Friend WithEvents lblUserName As Label
    Friend WithEvents lblListID As Label
    Friend WithEvents lblUserID As Label
    Friend WithEvents rdbPermanent As RadioButton
    Friend WithEvents rdbInduction As RadioButton
    Friend WithEvents txtUserID As TextBox
    Friend WithEvents txtListID As TextBox
    Friend WithEvents txtUserName As TextBox
End Class
