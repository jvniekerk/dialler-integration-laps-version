﻿Module modLists
    Sub getLists()
        Try
            Try
                Dim query As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Lists start' "
                Call ConnectToSql(query, False)
            Catch
            End Try

            Dim request As String = "<data clientid='10864'><System.BaseIndex /></data>"
            Dim un As String = "Int_Lists"
            Dim pw As String = "Unc13Buck"
            Dim response As String = GetUltraDownload(request, un, pw)
            'Call saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\ListsTest.txt", response)
            Dim rows As Integer = getRowNumbers(response)
            'MsgBox("rows: " & rows.ToString)
            Dim listID As Integer
            Dim listName As Integer
            Dim schemaName As Integer

            If rows > 0 Then
                Dim thisArray As String(,) = GetArrayFromXMLwithHeaders(response)
                Dim cols As Integer = thisArray.GetLength(1)
                rows = thisArray.GetLength(0)

                For i As Integer = 0 To cols - 1
                    If thisArray(0, i) = "BaseIndexId" Then
                        listID = i
                        'MsgBox(thisArray(0, i).ToString & " -> " & listID.ToString)
                    ElseIf thisArray(0, i) = "BaseName" Then
                        listName = i
                        'MsgBox(thisArray(0, i).ToString & " -> " & listName.ToString)
                    ElseIf thisArray(0, i) = "SchemaCode" Then
                        schemaName = i
                        'MsgBox(thisArray(0, i).ToString & " -> " & schemaName.ToString)
                    End If
                Next

                Dim query As String = " insert into DiallerStagingArea.dbo.UltraDownloadXML (DownloadType, DownloadText, DownloadDate, [Rows]) "
                query = query & " select 'Lists', '" & response & "', getdate(), " & rows & " "
                query = query & Environment.NewLine & " delete from DiallerStagingArea.dbo.Lists "
                query = query & Environment.NewLine & " insert into DiallerStagingArea.dbo.Lists (ListID, ListName, SchemaCode) values "
                For i As Integer = 1 To rows - 1
                    query = query & Environment.NewLine & "(" & thisArray(i, listID) & ", '" & thisArray(i, listName) & "', '" & thisArray(i, schemaName) & "') "
                    'MsgBox("BaseIndexId: " & thisArray(i, listID) & "; BaseName: " & thisArray(i, listName) & "; SchemaCode: " & thisArray(i, schemaName))

                    If i < rows - 1 Then
                        query = query & ", "
                    End If
                Next i
                'Call ConnectToSql(query, False)

                query = query & Environment.NewLine & "update DiallerStagingArea.dbo.Lists set Stream = case "
                query = query & "when ListName like '%Arrears Stream%' then 'Arrears Stream' "
                query = query & "when ListName like '%PreFund Stream%' then 'PreFund Stream' else 'Other' end "
                'MsgBox(query)
                Call ConnectToSql(query, False)
            End If

            Try
                Dim query As String = " insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Update Lists end' "
                Call ConnectToSql(query, False)
            Catch
            End Try
        Catch
        End Try
    End Sub
End Module
