﻿Module modDeleteOldAccounts
    Sub DeleteOldAccounts()
        Try
            Dim un As String
            Dim pw As String
            Dim schema As String = "StagingAreaIntegration"
            Dim request As String
            Dim response As String
            Dim loans As DataTable
            Dim query As String
            Dim listID As Integer
            Dim rowID As Integer
            Dim rows As Integer
            Dim loanNo As Integer

            un = "Int_Update"
            pw = "Unc13Buck"

            query = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) " & " select 'Deleting Old Accounts' "
            ConnectToSql(query, False)

            query = "select distinct c.ListID, c.RowID, c.LoanNo from DiallerStagingArea.dbo.CurrentAccounts c "
            query += "where c.DateModified < convert(date, dateadd(week, -1, getdate())) and c.StagingAreaStatus =4 "
            loans = sqlToDataTable(query)
            rows = loans.Rows.Count

            If rows > 0 Then
                If rows > 100 Then
                    rows = 100
                End If

                For i As Integer = 0 To rows - 1
                    listID = loans.Rows(i).Item(0)
                    rowID = loans.Rows(i).Item(1)
                    loanNo = loans.Rows(i).Item(2)

                    request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID.ToString & "' RowId='" & rowID.ToString & "'/></data>"
                    response = GetUltraDownload(request, un, pw)
                    'MsgBox(response)

                    If response.IndexOf("rows=""") > 0 Then
                        request = "<upload clientid='10864'><System.Base.Counters BaseIndexId='" & listID & "'><rows>"
                        request += "<DeleteOnly _RowId='" & rowID & "'/></rows></System.Base.Counters></upload>"
                        response = GetUltraUpload(request, un, pw)
                        'MsgBox(response)

                        If response.IndexOf("outcome=""ok") > 0 Then
                            query = "delete from DiallerStagingArea.dbo.CurrentAccounts where ListID = " & listID.ToString & " and RowID = " & rowID.ToString
                            ConnectToSql(query, False)
                            'MsgBox("deleted")
                        End If
                    End If
                Next i

                query = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) " & " select 'Deleted " & rows.ToString & " Old Accounts' "
                ConnectToSql(query, False)
            End If
        Catch
        End Try
    End Sub
End Module
