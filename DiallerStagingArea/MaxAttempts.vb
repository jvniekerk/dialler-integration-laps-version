﻿Option Explicit On

Module MaxAttempts
    Sub ResetMaxAttempts()
        Dim rowID As String = "95"
        Dim request As String = "test"
        Dim returnTable As DataTable
        Dim returnArray As String(,)
        Dim i As Integer = 0
        Dim listsTable As New DataTable
        Dim thisListID As Integer = 0
        Dim thisTable As DataTable
        Dim view As DataView
        Dim listFilter As String = ""
        Dim rows As Integer = 0
        Dim thisRow As Integer = 499
        Dim j As Integer = 0

        returnTable = (GetListsRows())
        returnArray = GetArrayFromDataTable(returnTable)
        listsTable = returnTable.DefaultView.ToTable(True, "ListID")

        For Each row As DataRow In listsTable.Rows
            thisListID = row.Item("ListID")
            listFilter = "ListID = '" & thisListID & "'"
            view = New DataView(returnTable, listFilter, "RowID", DataViewRowState.CurrentRows)
            thisTable = view.ToTable()
            rows = thisTable.Rows.Count
            thisRow = 0
            Dim rowCount As Integer = 0
            'MsgBox("ListID: " & thisListID.ToString & " -> " & rows.ToString & " rows")
            j = 0

            If rows > 500 Then
                'MsgBox("List: " & thisListID.ToString & " - Rows greater than 500")
                While j < rows
                    request = "<upload clientid='10864'>"
                    request = request & "<System.Base.Counters BaseIndexId='" & thisListID & "'>"
                    request = request & "<rows>"
                    rowCount = 0

                    For i = thisRow To thisRow + 499
                        If i < rows Then
                            rowID = thisTable.Rows(i).Item(1).ToString
                            request = request & "<DeleteOnly _RowId='" & rowID & "'/>"
                            rowCount = rowCount + 1
                        End If
                        j = j + 1
                    Next i

                    request = request & "</rows>"
                    request = request & "</System.Base.Counters>"
                    request = request & "</upload>"

                    Call ProcessMaxAttempts(request, thisListID, rowCount)

                    thisRow = thisRow + 500
                End While
            Else
                'MsgBox("List: " & thisListID.ToString & " - Rows less than 500")
                request = "<upload clientid='10864'>"
                request = request & "<System.Base.Counters BaseIndexId='" & thisListID & "'>"
                request = request & "<rows>"

                For i = 0 To rows - 1
                    rowID = thisTable.Rows(i).Item(1).ToString
                    request = request & "<DeleteOnly _RowId='" & rowID & "'/>"
                Next i

                request = request & "</rows>"
                request = request & "</System.Base.Counters>"
                request = request & "</upload>"

                Call ProcessMaxAttempts(request, thisListID, rows)
            End If
        Next row

        Dim query As String = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Max Attempts Reset'"
        Call ConnectToSql(query, False)

        view = Nothing
        thisTable = Nothing
        returnTable = Nothing
    End Sub

    Function GetListsRows() As DataTable
        Dim query As String = "select distinct ListID, RowID from DiallerStagingArea.dbo.CurrentAccounts order by ListID, RowID"
        Dim result As DataTable = sqlToDataTable(query)
        Return result
    End Function

    Sub ProcessMaxAttempts(ByVal request As String, listID As Integer, rows As Integer)
        Dim batchID As Integer = 0
        Dim query As String = "select convert(varchar(20), isnull(max(ChangeBatchID), 0) +1) as batchID from DiallerStagingArea.dbo.UltraRequest"
        Dim response As String = ""
        Dim un As String = "Int_Max"
        Dim pw As String = "Unc13Buck"
        'MsgBox(request)

        batchID = ConnectToSql(query, False)
        'MsgBox("BatchID: " & batchID)

        'Save request to UB db
        response = request.Replace("'", """")
        query = "insert into DiallerStagingArea.dbo.UltraRequest (DateCreated, ChangeBatchID, ListID, [TimeStamp], RequestXML) "
        query = query & "select getdate() as DateCreated, " & batchID & " as ChangeBatchID, " & listID & " as ListID "
        query = query & ", replace(replace(replace(replace(convert(varchar(20), getdate(), 120), '-', ''), ':', ''), '.', ''), ' ', '_') as [TimeStamp] "
        query = query & ", '" & response & "' as RequestXML"
        'MsgBox(query)
        Call ConnectToSql(query, False)

        'Send request to Ultra
        request = GetUltraUpload(request, un, pw)

        'Save response to UB db
        response = request.Replace("'", """")
        query = "insert into DiallerStagingArea.dbo.UltraResponse (DateCreated, ChangeBatchID, ListID, [TimeStamp], ResponseXML) "
        query = query & "select getdate() as DateCreated, " & batchID & " as ChangeBatchID, " & listID & " as ListID "
        query = query & ", replace(replace(replace(replace(convert(varchar(20), getdate(), 120), '-', ''), ':', ''), '.', ''), ' ', '_') as [TimeStamp] "
        query = query & ", '" & response & "' as ResponseXML"
        'MsgBox(query)
        Call ConnectToSql(query, False)

        'Store Max Attempts Reset batch
        query = "insert into DiallerStagingArea.dbo.MaxAttemptsReset ( ListID, RowsReset, BatchID ) "
        query = query & "Select " & listID.ToString & ", " & rows.ToString & ", " & batchID.ToString
        Call ConnectToSql(query, False)
    End Sub
End Module
