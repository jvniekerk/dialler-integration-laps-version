﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.btnRun = New System.Windows.Forms.Button()
        Me.btnExit = New System.Windows.Forms.Button()
        Me.btnStop = New System.Windows.Forms.Button()
        Me.bgwUpdateDialler = New System.ComponentModel.BackgroundWorker()
        Me.cmbTest = New System.Windows.Forms.Button()
        Me.btnRebuildTables = New System.Windows.Forms.Button()
        Me.chkMonday = New System.Windows.Forms.CheckBox()
        Me.chkTuesday = New System.Windows.Forms.CheckBox()
        Me.chkWednesday = New System.Windows.Forms.CheckBox()
        Me.chkThursday = New System.Windows.Forms.CheckBox()
        Me.chkFriday = New System.Windows.Forms.CheckBox()
        Me.chkSaturday = New System.Windows.Forms.CheckBox()
        Me.chkSunday = New System.Windows.Forms.CheckBox()
        Me.lblDay = New System.Windows.Forms.Label()
        Me.lblStartTime = New System.Windows.Forms.Label()
        Me.txtMonStart = New System.Windows.Forms.TextBox()
        Me.txtTueStart = New System.Windows.Forms.TextBox()
        Me.txtWedStart = New System.Windows.Forms.TextBox()
        Me.txtThuStart = New System.Windows.Forms.TextBox()
        Me.txtFriStart = New System.Windows.Forms.TextBox()
        Me.txtSatStart = New System.Windows.Forms.TextBox()
        Me.txtSunStart = New System.Windows.Forms.TextBox()
        Me.txtSunEnd = New System.Windows.Forms.TextBox()
        Me.txtSatEnd = New System.Windows.Forms.TextBox()
        Me.txtFriEnd = New System.Windows.Forms.TextBox()
        Me.txtThuEnd = New System.Windows.Forms.TextBox()
        Me.txtWedEnd = New System.Windows.Forms.TextBox()
        Me.txtTueEnd = New System.Windows.Forms.TextBox()
        Me.txtMonEnd = New System.Windows.Forms.TextBox()
        Me.lblEndTime = New System.Windows.Forms.Label()
        Me.pnlDiallerTimes = New System.Windows.Forms.Panel()
        Me.btnAddUser = New System.Windows.Forms.Button()
        Me.lblProgress = New System.Windows.Forms.Label()
        Me.tmrElapsedTime = New System.Windows.Forms.Timer(Me.components)
        Me.lblCurrDate = New System.Windows.Forms.Label()
        Me.tmrCurrDate = New System.Windows.Forms.Timer(Me.components)
        Me.bgwUnderwriting = New System.ComponentModel.BackgroundWorker()
        Me.bgwCallAttempts = New System.ComponentModel.BackgroundWorker()
        Me.bgwCollections = New System.ComponentModel.BackgroundWorker()
        Me.btnLists = New System.Windows.Forms.Button()
        Me.bgwAccountStateCheck = New System.ComponentModel.BackgroundWorker()
        Me.txtDownloadStart = New System.Windows.Forms.TextBox()
        Me.txtDownloadEnd = New System.Windows.Forms.TextBox()
        Me.gpbDownloadTimes = New System.Windows.Forms.GroupBox()
        Me.lblDownloadEnd = New System.Windows.Forms.Label()
        Me.lblDownloadStart = New System.Windows.Forms.Label()
        Me.gpbUpdateProgress = New System.Windows.Forms.GroupBox()
        Me.lsbUpdateProgress = New System.Windows.Forms.ListBox()
        Me.gpbDownloadProgress = New System.Windows.Forms.GroupBox()
        Me.lsbDownloadProgress = New System.Windows.Forms.ListBox()
        Me.pnlDiallerTimes.SuspendLayout()
        Me.gpbDownloadTimes.SuspendLayout()
        Me.gpbUpdateProgress.SuspendLayout()
        Me.gpbDownloadProgress.SuspendLayout()
        Me.SuspendLayout()
        '
        'btnRun
        '
        Me.btnRun.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnRun.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnRun.FlatAppearance.BorderSize = 5
        Me.btnRun.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnRun.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnRun.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnRun.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRun.Location = New System.Drawing.Point(12, 12)
        Me.btnRun.Name = "btnRun"
        Me.btnRun.Size = New System.Drawing.Size(287, 58)
        Me.btnRun.TabIndex = 0
        Me.btnRun.Text = "Run Dialler Update"
        Me.btnRun.UseVisualStyleBackColor = False
        '
        'btnExit
        '
        Me.btnExit.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnExit.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnExit.FlatAppearance.BorderSize = 5
        Me.btnExit.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnExit.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnExit.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnExit.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnExit.Location = New System.Drawing.Point(307, 140)
        Me.btnExit.Name = "btnExit"
        Me.btnExit.Size = New System.Drawing.Size(287, 58)
        Me.btnExit.TabIndex = 1
        Me.btnExit.Text = "Exit Application"
        Me.btnExit.UseVisualStyleBackColor = False
        '
        'btnStop
        '
        Me.btnStop.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnStop.Enabled = False
        Me.btnStop.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnStop.FlatAppearance.BorderSize = 5
        Me.btnStop.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnStop.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnStop.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnStop.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnStop.Location = New System.Drawing.Point(12, 76)
        Me.btnStop.Name = "btnStop"
        Me.btnStop.Size = New System.Drawing.Size(287, 58)
        Me.btnStop.TabIndex = 2
        Me.btnStop.Text = "Stop Dialler Update"
        Me.btnStop.UseVisualStyleBackColor = False
        '
        'bgwUpdateDialler
        '
        Me.bgwUpdateDialler.WorkerReportsProgress = True
        Me.bgwUpdateDialler.WorkerSupportsCancellation = True
        '
        'cmbTest
        '
        Me.cmbTest.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.cmbTest.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.cmbTest.Location = New System.Drawing.Point(309, 76)
        Me.cmbTest.Name = "cmbTest"
        Me.cmbTest.Size = New System.Drawing.Size(283, 58)
        Me.cmbTest.TabIndex = 3
        Me.cmbTest.Text = "Run Test"
        Me.cmbTest.UseVisualStyleBackColor = False
        '
        'btnRebuildTables
        '
        Me.btnRebuildTables.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnRebuildTables.Enabled = False
        Me.btnRebuildTables.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnRebuildTables.FlatAppearance.BorderSize = 5
        Me.btnRebuildTables.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnRebuildTables.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnRebuildTables.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnRebuildTables.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnRebuildTables.Location = New System.Drawing.Point(14, 140)
        Me.btnRebuildTables.Name = "btnRebuildTables"
        Me.btnRebuildTables.Size = New System.Drawing.Size(287, 58)
        Me.btnRebuildTables.TabIndex = 7
        Me.btnRebuildTables.Text = "Rebuild DB Tables"
        Me.btnRebuildTables.UseVisualStyleBackColor = False
        '
        'chkMonday
        '
        Me.chkMonday.AutoSize = True
        Me.chkMonday.Checked = True
        Me.chkMonday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkMonday.Location = New System.Drawing.Point(27, 59)
        Me.chkMonday.Name = "chkMonday"
        Me.chkMonday.Size = New System.Drawing.Size(163, 44)
        Me.chkMonday.TabIndex = 12
        Me.chkMonday.Text = "Monday"
        Me.chkMonday.UseVisualStyleBackColor = True
        '
        'chkTuesday
        '
        Me.chkTuesday.AutoSize = True
        Me.chkTuesday.Checked = True
        Me.chkTuesday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkTuesday.Location = New System.Drawing.Point(28, 113)
        Me.chkTuesday.Name = "chkTuesday"
        Me.chkTuesday.Size = New System.Drawing.Size(165, 44)
        Me.chkTuesday.TabIndex = 13
        Me.chkTuesday.Text = "Tuesday"
        Me.chkTuesday.UseVisualStyleBackColor = True
        '
        'chkWednesday
        '
        Me.chkWednesday.AutoSize = True
        Me.chkWednesday.Checked = True
        Me.chkWednesday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkWednesday.Location = New System.Drawing.Point(28, 166)
        Me.chkWednesday.Name = "chkWednesday"
        Me.chkWednesday.Size = New System.Drawing.Size(208, 44)
        Me.chkWednesday.TabIndex = 14
        Me.chkWednesday.Text = "Wednesday"
        Me.chkWednesday.UseVisualStyleBackColor = True
        '
        'chkThursday
        '
        Me.chkThursday.AutoSize = True
        Me.chkThursday.Checked = True
        Me.chkThursday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkThursday.Location = New System.Drawing.Point(27, 219)
        Me.chkThursday.Name = "chkThursday"
        Me.chkThursday.Size = New System.Drawing.Size(182, 44)
        Me.chkThursday.TabIndex = 15
        Me.chkThursday.Text = "Thursday"
        Me.chkThursday.UseVisualStyleBackColor = True
        '
        'chkFriday
        '
        Me.chkFriday.AutoSize = True
        Me.chkFriday.Checked = True
        Me.chkFriday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkFriday.Location = New System.Drawing.Point(27, 275)
        Me.chkFriday.Name = "chkFriday"
        Me.chkFriday.Size = New System.Drawing.Size(142, 44)
        Me.chkFriday.TabIndex = 16
        Me.chkFriday.Text = "Friday"
        Me.chkFriday.UseVisualStyleBackColor = True
        '
        'chkSaturday
        '
        Me.chkSaturday.AutoSize = True
        Me.chkSaturday.Checked = True
        Me.chkSaturday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSaturday.Location = New System.Drawing.Point(28, 325)
        Me.chkSaturday.Name = "chkSaturday"
        Me.chkSaturday.Size = New System.Drawing.Size(183, 44)
        Me.chkSaturday.TabIndex = 17
        Me.chkSaturday.Text = "Saturday"
        Me.chkSaturday.UseVisualStyleBackColor = True
        '
        'chkSunday
        '
        Me.chkSunday.AutoSize = True
        Me.chkSunday.Checked = True
        Me.chkSunday.CheckState = System.Windows.Forms.CheckState.Checked
        Me.chkSunday.Location = New System.Drawing.Point(27, 378)
        Me.chkSunday.Name = "chkSunday"
        Me.chkSunday.Size = New System.Drawing.Size(164, 44)
        Me.chkSunday.TabIndex = 18
        Me.chkSunday.Text = "Sunday"
        Me.chkSunday.UseVisualStyleBackColor = True
        '
        'lblDay
        '
        Me.lblDay.AutoSize = True
        Me.lblDay.Font = New System.Drawing.Font("Bradley Hand ITC", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblDay.Location = New System.Drawing.Point(34, 0)
        Me.lblDay.Name = "lblDay"
        Me.lblDay.Size = New System.Drawing.Size(101, 54)
        Me.lblDay.TabIndex = 19
        Me.lblDay.Text = "Day"
        '
        'lblStartTime
        '
        Me.lblStartTime.AutoSize = True
        Me.lblStartTime.Font = New System.Drawing.Font("Bradley Hand ITC", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblStartTime.Location = New System.Drawing.Point(238, 2)
        Me.lblStartTime.Name = "lblStartTime"
        Me.lblStartTime.Size = New System.Drawing.Size(232, 54)
        Me.lblStartTime.TabIndex = 20
        Me.lblStartTime.Text = "Start Time"
        '
        'txtMonStart
        '
        Me.txtMonStart.Location = New System.Drawing.Point(232, 59)
        Me.txtMonStart.Name = "txtMonStart"
        Me.txtMonStart.Size = New System.Drawing.Size(159, 47)
        Me.txtMonStart.TabIndex = 21
        Me.txtMonStart.Text = "8:00"
        Me.txtMonStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTueStart
        '
        Me.txtTueStart.Location = New System.Drawing.Point(232, 112)
        Me.txtTueStart.Name = "txtTueStart"
        Me.txtTueStart.Size = New System.Drawing.Size(159, 47)
        Me.txtTueStart.TabIndex = 22
        Me.txtTueStart.Text = "8:00"
        Me.txtTueStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtWedStart
        '
        Me.txtWedStart.Location = New System.Drawing.Point(232, 165)
        Me.txtWedStart.Name = "txtWedStart"
        Me.txtWedStart.Size = New System.Drawing.Size(159, 47)
        Me.txtWedStart.TabIndex = 23
        Me.txtWedStart.Text = "8:00"
        Me.txtWedStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtThuStart
        '
        Me.txtThuStart.Location = New System.Drawing.Point(232, 218)
        Me.txtThuStart.Name = "txtThuStart"
        Me.txtThuStart.Size = New System.Drawing.Size(159, 47)
        Me.txtThuStart.TabIndex = 24
        Me.txtThuStart.Text = "8:00"
        Me.txtThuStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFriStart
        '
        Me.txtFriStart.Location = New System.Drawing.Point(232, 271)
        Me.txtFriStart.Name = "txtFriStart"
        Me.txtFriStart.Size = New System.Drawing.Size(159, 47)
        Me.txtFriStart.TabIndex = 25
        Me.txtFriStart.Text = "8:00"
        Me.txtFriStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSatStart
        '
        Me.txtSatStart.Location = New System.Drawing.Point(232, 324)
        Me.txtSatStart.Name = "txtSatStart"
        Me.txtSatStart.Size = New System.Drawing.Size(159, 47)
        Me.txtSatStart.TabIndex = 26
        Me.txtSatStart.Text = "9:00"
        Me.txtSatStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSunStart
        '
        Me.txtSunStart.Location = New System.Drawing.Point(232, 377)
        Me.txtSunStart.Name = "txtSunStart"
        Me.txtSunStart.Size = New System.Drawing.Size(159, 47)
        Me.txtSunStart.TabIndex = 27
        Me.txtSunStart.Text = "9:00"
        Me.txtSunStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSunEnd
        '
        Me.txtSunEnd.Location = New System.Drawing.Point(449, 377)
        Me.txtSunEnd.Name = "txtSunEnd"
        Me.txtSunEnd.Size = New System.Drawing.Size(159, 47)
        Me.txtSunEnd.TabIndex = 35
        Me.txtSunEnd.Text = "18:00"
        Me.txtSunEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtSatEnd
        '
        Me.txtSatEnd.Location = New System.Drawing.Point(449, 324)
        Me.txtSatEnd.Name = "txtSatEnd"
        Me.txtSatEnd.Size = New System.Drawing.Size(159, 47)
        Me.txtSatEnd.TabIndex = 34
        Me.txtSatEnd.Text = "18:00"
        Me.txtSatEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtFriEnd
        '
        Me.txtFriEnd.Location = New System.Drawing.Point(449, 271)
        Me.txtFriEnd.Name = "txtFriEnd"
        Me.txtFriEnd.Size = New System.Drawing.Size(159, 47)
        Me.txtFriEnd.TabIndex = 33
        Me.txtFriEnd.Text = "21:30"
        Me.txtFriEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtThuEnd
        '
        Me.txtThuEnd.Location = New System.Drawing.Point(449, 218)
        Me.txtThuEnd.Name = "txtThuEnd"
        Me.txtThuEnd.Size = New System.Drawing.Size(159, 47)
        Me.txtThuEnd.TabIndex = 32
        Me.txtThuEnd.Text = "21:30"
        Me.txtThuEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtWedEnd
        '
        Me.txtWedEnd.Location = New System.Drawing.Point(449, 165)
        Me.txtWedEnd.Name = "txtWedEnd"
        Me.txtWedEnd.Size = New System.Drawing.Size(159, 47)
        Me.txtWedEnd.TabIndex = 31
        Me.txtWedEnd.Text = "21:30"
        Me.txtWedEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtTueEnd
        '
        Me.txtTueEnd.Location = New System.Drawing.Point(449, 112)
        Me.txtTueEnd.Name = "txtTueEnd"
        Me.txtTueEnd.Size = New System.Drawing.Size(159, 47)
        Me.txtTueEnd.TabIndex = 30
        Me.txtTueEnd.Text = "21:30"
        Me.txtTueEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtMonEnd
        '
        Me.txtMonEnd.Location = New System.Drawing.Point(449, 59)
        Me.txtMonEnd.Name = "txtMonEnd"
        Me.txtMonEnd.Size = New System.Drawing.Size(159, 47)
        Me.txtMonEnd.TabIndex = 29
        Me.txtMonEnd.Text = "21:30"
        Me.txtMonEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'lblEndTime
        '
        Me.lblEndTime.AutoSize = True
        Me.lblEndTime.Font = New System.Drawing.Font("Bradley Hand ITC", 16.125!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lblEndTime.Location = New System.Drawing.Point(455, 2)
        Me.lblEndTime.Name = "lblEndTime"
        Me.lblEndTime.Size = New System.Drawing.Size(209, 54)
        Me.lblEndTime.TabIndex = 28
        Me.lblEndTime.Text = "End Time"
        '
        'pnlDiallerTimes
        '
        Me.pnlDiallerTimes.Controls.Add(Me.txtFriEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSunEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkMonday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSatEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkTuesday)
        Me.pnlDiallerTimes.Controls.Add(Me.chkWednesday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtThuEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkThursday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtWedEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkFriday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtTueEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkSaturday)
        Me.pnlDiallerTimes.Controls.Add(Me.txtMonEnd)
        Me.pnlDiallerTimes.Controls.Add(Me.chkSunday)
        Me.pnlDiallerTimes.Controls.Add(Me.lblEndTime)
        Me.pnlDiallerTimes.Controls.Add(Me.lblDay)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSunStart)
        Me.pnlDiallerTimes.Controls.Add(Me.lblStartTime)
        Me.pnlDiallerTimes.Controls.Add(Me.txtSatStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtMonStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtFriStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtTueStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtThuStart)
        Me.pnlDiallerTimes.Controls.Add(Me.txtWedStart)
        Me.pnlDiallerTimes.Location = New System.Drawing.Point(7, 321)
        Me.pnlDiallerTimes.Name = "pnlDiallerTimes"
        Me.pnlDiallerTimes.Size = New System.Drawing.Size(635, 443)
        Me.pnlDiallerTimes.TabIndex = 36
        '
        'btnAddUser
        '
        Me.btnAddUser.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnAddUser.Enabled = False
        Me.btnAddUser.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnAddUser.FlatAppearance.BorderSize = 5
        Me.btnAddUser.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnAddUser.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnAddUser.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnAddUser.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnAddUser.Location = New System.Drawing.Point(305, 12)
        Me.btnAddUser.Name = "btnAddUser"
        Me.btnAddUser.Size = New System.Drawing.Size(287, 58)
        Me.btnAddUser.TabIndex = 37
        Me.btnAddUser.Text = "Manage Agents"
        Me.btnAddUser.UseVisualStyleBackColor = False
        '
        'lblProgress
        '
        Me.lblProgress.AutoSize = True
        Me.lblProgress.Location = New System.Drawing.Point(7, 278)
        Me.lblProgress.Name = "lblProgress"
        Me.lblProgress.Size = New System.Drawing.Size(673, 40)
        Me.lblProgress.TabIndex = 38
        Me.lblProgress.Text = "Dialler Update Time Lapsed: 0 minutes 0 seconds"
        '
        'tmrElapsedTime
        '
        Me.tmrElapsedTime.Interval = 1000
        '
        'lblCurrDate
        '
        Me.lblCurrDate.AutoSize = True
        Me.lblCurrDate.Location = New System.Drawing.Point(449, 278)
        Me.lblCurrDate.Name = "lblCurrDate"
        Me.lblCurrDate.Size = New System.Drawing.Size(75, 40)
        Me.lblCurrDate.TabIndex = 39
        Me.lblCurrDate.Text = "Date"
        '
        'tmrCurrDate
        '
        Me.tmrCurrDate.Enabled = True
        Me.tmrCurrDate.Interval = 1000
        '
        'bgwUnderwriting
        '
        Me.bgwUnderwriting.WorkerReportsProgress = True
        Me.bgwUnderwriting.WorkerSupportsCancellation = True
        '
        'bgwCallAttempts
        '
        Me.bgwCallAttempts.WorkerReportsProgress = True
        Me.bgwCallAttempts.WorkerSupportsCancellation = True
        '
        'bgwCollections
        '
        Me.bgwCollections.WorkerReportsProgress = True
        Me.bgwCollections.WorkerSupportsCancellation = True
        '
        'btnLists
        '
        Me.btnLists.BackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnLists.FlatAppearance.BorderColor = System.Drawing.Color.Gray
        Me.btnLists.FlatAppearance.BorderSize = 5
        Me.btnLists.FlatAppearance.CheckedBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.btnLists.FlatAppearance.MouseDownBackColor = System.Drawing.Color.FromArgb(CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer), CType(CType(224, Byte), Integer))
        Me.btnLists.FlatAppearance.MouseOverBackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.btnLists.FlatStyle = System.Windows.Forms.FlatStyle.Popup
        Me.btnLists.Location = New System.Drawing.Point(14, 204)
        Me.btnLists.Name = "btnLists"
        Me.btnLists.Size = New System.Drawing.Size(287, 58)
        Me.btnLists.TabIndex = 40
        Me.btnLists.Text = "Rebuild Lists Table"
        Me.btnLists.UseVisualStyleBackColor = False
        '
        'bgwAccountStateCheck
        '
        '
        'txtDownloadStart
        '
        Me.txtDownloadStart.Location = New System.Drawing.Point(71, 31)
        Me.txtDownloadStart.Name = "txtDownloadStart"
        Me.txtDownloadStart.Size = New System.Drawing.Size(78, 47)
        Me.txtDownloadStart.TabIndex = 22
        Me.txtDownloadStart.Text = "5:00"
        Me.txtDownloadStart.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'txtDownloadEnd
        '
        Me.txtDownloadEnd.Location = New System.Drawing.Point(221, 31)
        Me.txtDownloadEnd.Name = "txtDownloadEnd"
        Me.txtDownloadEnd.Size = New System.Drawing.Size(78, 47)
        Me.txtDownloadEnd.TabIndex = 23
        Me.txtDownloadEnd.Text = "22:30"
        Me.txtDownloadEnd.TextAlign = System.Windows.Forms.HorizontalAlignment.Center
        '
        'gpbDownloadTimes
        '
        Me.gpbDownloadTimes.Controls.Add(Me.lblDownloadEnd)
        Me.gpbDownloadTimes.Controls.Add(Me.lblDownloadStart)
        Me.gpbDownloadTimes.Controls.Add(Me.txtDownloadEnd)
        Me.gpbDownloadTimes.Controls.Add(Me.txtDownloadStart)
        Me.gpbDownloadTimes.Font = New System.Drawing.Font("Bradley Hand ITC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.gpbDownloadTimes.Location = New System.Drawing.Point(309, 204)
        Me.gpbDownloadTimes.Name = "gpbDownloadTimes"
        Me.gpbDownloadTimes.Size = New System.Drawing.Size(315, 71)
        Me.gpbDownloadTimes.TabIndex = 42
        Me.gpbDownloadTimes.TabStop = False
        Me.gpbDownloadTimes.Text = "Data Download Times"
        '
        'lblDownloadEnd
        '
        Me.lblDownloadEnd.AutoSize = True
        Me.lblDownloadEnd.Location = New System.Drawing.Point(155, 34)
        Me.lblDownloadEnd.Name = "lblDownloadEnd"
        Me.lblDownloadEnd.Size = New System.Drawing.Size(82, 40)
        Me.lblDownloadEnd.TabIndex = 44
        Me.lblDownloadEnd.Text = "End:"
        '
        'lblDownloadStart
        '
        Me.lblDownloadStart.AutoSize = True
        Me.lblDownloadStart.Location = New System.Drawing.Point(6, 35)
        Me.lblDownloadStart.Name = "lblDownloadStart"
        Me.lblDownloadStart.Size = New System.Drawing.Size(98, 40)
        Me.lblDownloadStart.TabIndex = 43
        Me.lblDownloadStart.Text = "Start:"
        '
        'gpbUpdateProgress
        '
        Me.gpbUpdateProgress.Controls.Add(Me.lsbUpdateProgress)
        Me.gpbUpdateProgress.Location = New System.Drawing.Point(686, 10)
        Me.gpbUpdateProgress.Name = "gpbUpdateProgress"
        Me.gpbUpdateProgress.Size = New System.Drawing.Size(266, 733)
        Me.gpbUpdateProgress.TabIndex = 43
        Me.gpbUpdateProgress.TabStop = False
        Me.gpbUpdateProgress.Text = "Dialler Update Progress"
        '
        'lsbUpdateProgress
        '
        Me.lsbUpdateProgress.Enabled = False
        Me.lsbUpdateProgress.Font = New System.Drawing.Font("Bradley Hand ITC", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsbUpdateProgress.FormattingEnabled = True
        Me.lsbUpdateProgress.ItemHeight = 34
        Me.lsbUpdateProgress.Location = New System.Drawing.Point(0, 98)
        Me.lsbUpdateProgress.Name = "lsbUpdateProgress"
        Me.lsbUpdateProgress.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lsbUpdateProgress.Size = New System.Drawing.Size(256, 616)
        Me.lsbUpdateProgress.TabIndex = 0
        '
        'gpbDownloadProgress
        '
        Me.gpbDownloadProgress.Controls.Add(Me.lsbDownloadProgress)
        Me.gpbDownloadProgress.Location = New System.Drawing.Point(984, 10)
        Me.gpbDownloadProgress.Name = "gpbDownloadProgress"
        Me.gpbDownloadProgress.Size = New System.Drawing.Size(266, 733)
        Me.gpbDownloadProgress.TabIndex = 44
        Me.gpbDownloadProgress.TabStop = False
        Me.gpbDownloadProgress.Text = "Data Download Progress"
        '
        'lsbDownloadProgress
        '
        Me.lsbDownloadProgress.Enabled = False
        Me.lsbDownloadProgress.Font = New System.Drawing.Font("Bradley Hand ITC", 10.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.lsbDownloadProgress.FormattingEnabled = True
        Me.lsbDownloadProgress.ItemHeight = 34
        Me.lsbDownloadProgress.Location = New System.Drawing.Point(0, 98)
        Me.lsbDownloadProgress.Name = "lsbDownloadProgress"
        Me.lsbDownloadProgress.SelectionMode = System.Windows.Forms.SelectionMode.None
        Me.lsbDownloadProgress.Size = New System.Drawing.Size(256, 616)
        Me.lsbDownloadProgress.TabIndex = 1
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(18.0!, 40.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoScroll = True
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(192, Byte), Integer), CType(CType(255, Byte), Integer), CType(CType(192, Byte), Integer))
        Me.ClientSize = New System.Drawing.Size(1264, 780)
        Me.Controls.Add(Me.gpbDownloadProgress)
        Me.Controls.Add(Me.gpbUpdateProgress)
        Me.Controls.Add(Me.gpbDownloadTimes)
        Me.Controls.Add(Me.btnLists)
        Me.Controls.Add(Me.lblCurrDate)
        Me.Controls.Add(Me.lblProgress)
        Me.Controls.Add(Me.btnAddUser)
        Me.Controls.Add(Me.pnlDiallerTimes)
        Me.Controls.Add(Me.btnRebuildTables)
        Me.Controls.Add(Me.cmbTest)
        Me.Controls.Add(Me.btnStop)
        Me.Controls.Add(Me.btnExit)
        Me.Controls.Add(Me.btnRun)
        Me.Font = New System.Drawing.Font("Bradley Hand ITC", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.Margin = New System.Windows.Forms.Padding(4, 5, 4, 5)
        Me.Name = "Form1"
        Me.RightToLeftLayout = True
        Me.Text = "Dialler Staging Area"
        Me.pnlDiallerTimes.ResumeLayout(False)
        Me.pnlDiallerTimes.PerformLayout()
        Me.gpbDownloadTimes.ResumeLayout(False)
        Me.gpbDownloadTimes.PerformLayout()
        Me.gpbUpdateProgress.ResumeLayout(False)
        Me.gpbDownloadProgress.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents btnRun As Button
    Friend WithEvents btnExit As Button
    Friend WithEvents btnStop As Button
    Friend WithEvents bgwUpdateDialler As System.ComponentModel.BackgroundWorker
    Friend WithEvents Button1 As Button
    Friend WithEvents cmbTest As Button
    Friend WithEvents btnRebuildTables As Button
    Friend WithEvents chkMonday As CheckBox
    Friend WithEvents chkTuesday As CheckBox
    Friend WithEvents chkWednesday As CheckBox
    Friend WithEvents chkThursday As CheckBox
    Friend WithEvents chkFriday As CheckBox
    Friend WithEvents chkSaturday As CheckBox
    Friend WithEvents chkSunday As CheckBox
    Friend WithEvents lblDay As Label
    Friend WithEvents lblStartTime As Label
    Friend WithEvents txtMonStart As TextBox
    Friend WithEvents txtTueStart As TextBox
    Friend WithEvents txtWedStart As TextBox
    Friend WithEvents txtThuStart As TextBox
    Friend WithEvents txtFriStart As TextBox
    Friend WithEvents txtSatStart As TextBox
    Friend WithEvents txtSunStart As TextBox
    Friend WithEvents txtSunEnd As TextBox
    Friend WithEvents txtSatEnd As TextBox
    Friend WithEvents txtFriEnd As TextBox
    Friend WithEvents txtThuEnd As TextBox
    Friend WithEvents txtWedEnd As TextBox
    Friend WithEvents txtTueEnd As TextBox
    Friend WithEvents txtMonEnd As TextBox
    Friend WithEvents lblEndTime As Label
    Friend WithEvents pnlDiallerTimes As Panel
    Friend WithEvents btnAddUser As Button
    Friend WithEvents lblProgress As Label
    Friend WithEvents tmrElapsedTime As Timer
    Friend WithEvents lblCurrDate As Label
    Friend WithEvents tmrCurrDate As Timer
    Friend WithEvents bgwUnderwriting As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwCallAttempts As System.ComponentModel.BackgroundWorker
    Friend WithEvents bgwCollections As System.ComponentModel.BackgroundWorker
    Friend WithEvents btnLists As Button
    Friend WithEvents bgwAccountStateCheck As System.ComponentModel.BackgroundWorker
    Friend WithEvents txtDownloadEnd As TextBox
    Friend WithEvents txtDownloadStart As TextBox
    Friend WithEvents gpbDownloadTimes As GroupBox
    Friend WithEvents lblDownloadEnd As Label
    Friend WithEvents lblDownloadStart As Label
    Friend WithEvents gpbUpdateProgress As GroupBox
    Friend WithEvents gpbDownloadProgress As GroupBox
    Public WithEvents lsbUpdateProgress As ListBox
    Public WithEvents lsbDownloadProgress As ListBox
End Class
