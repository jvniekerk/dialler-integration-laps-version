﻿Module modUpdateRowIDs
    Sub UpdateRowIDs()
        Try
            Dim un As String
            Dim pw As String
            Dim schema As String = "StagingAreaIntegration"
            Dim request As String
            Dim response As String
            Dim loans As DataTable
            Dim query As String
            Dim listID As Integer
            Dim rowID As Integer
            Dim rows As Integer
            Dim loanNo As Integer

            query = "select c.ListID, c.LoanNo from DiallerStagingArea.dbo.CurrentAccounts c where c.RowID =0"
            'query = "select top 3 c.ListID, c.LoanNo from DiallerStagingArea.dbo.CurrentAccounts c where c.ListID = 191 and c.StagingAreaStatus =2"
            loans = sqlToDataTable(query)
            rows = loans.Rows.Count

            un = "Int_Update"
            pw = "Unc13Buck"

            If rows > 0 Then
                'MsgBox("Rows: " & rows.ToString)

                For Each row As DataRow In loans.Rows
                    rowID = 0
                    listID = row.Item(0)
                    loanNo = row.Item(1)
                    request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID.ToString & "' LoanNo='" & loanNo.ToString & "'/></data>"
                    response = GetUltraDownload(request, un, pw)
                    'MsgBox(response)
                    rowID = GetRowIdFromXML(response, "RowId")

                    If rowID > 0 Then
                        'MsgBox("ListID: " & listID.ToString & "; LoanNo: " & loanNo.ToString & "; RowID: " & rowID.ToString)

                        query = "update c set RowID = " & rowID.ToString & " from DiallerStagingArea.dbo.CurrentAccounts c"
                        query += " where c.ListID = " & listID.ToString & " and c.LoanNo = " & loanNo.ToString & " "
                        'MsgBox(query)
                        Call ConnectToSql(query, False)
                    Else
                        'MsgBox("ListID: " & listID.ToString & " & LoanNo: " & loanNo.ToString & " -> RowID is zero!!")
                        query = "delete from DiallerStagingArea.dbo.CurrentAccounts where ListID = " & listID.ToString
                        query += " and LoanNo = " & loanNo.ToString & " and RowID =0"
                        Call ConnectToSql(query, False)
                    End If
                Next row

                query = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) " & " select 'Zero RowIDs fixed' "
                Call ConnectToSql(query, False)
                'Else
                '    MsgBox("No rows returned")
            End If
        Catch
        End Try
    End Sub
End Module
