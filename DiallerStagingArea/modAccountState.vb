﻿Module modAccountState
    Sub RunAccountStateCheck()
        Try
            Dim un As String = "Int_State"
            Dim pw As String = "Unc13Buck"
            Dim listID As Integer
            Dim rowID As Integer
            Dim dbState As Integer
            Dim ultraState As Integer
            Dim inUltra As Integer
            Dim schema As String = "StagingAreaIntegration"
            Dim request As String
            Dim response As String
            Dim query As String = "exec DiallerStagingArea.dbo.[sp_AccountsToCompare_Get]"
            Dim thisTable As DataTable = sqlToDataTable(query)
            Dim rows As Integer = thisTable.Rows.Count()
            Dim returnTable As DataTable = thisTable.Clone
            Dim rowsToRun As Integer = 500
            Dim rowsPerRun As Integer = rowsToRun
            Dim thisStart As DateTime = Now()

            'MsgBox("rows: " & rows)


            'ListID (0), RowID (1), DB_State (2), UltraState (3), InUltra (4)
            For i As Integer = 0 To rows - 1
                listID = thisTable.Rows(i).Item(0)
                rowID = thisTable.Rows(i).Item(1)
                dbState = thisTable.Rows(i).Item(2)
                ultraState = thisTable.Rows(i).Item(3)
                inUltra = 1

                request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID & "' RowId='" & rowID & "'/></data>"
                'MsgBox("<= GET DATA REQUEST => " & request)
                response = GetUltraDownload(request, un, pw)
                'MsgBox("<= GET DATA RESPONSE - ListID: " & listID.ToString & " - RowID: " & rowID.ToString & " => " & response)

                If response.IndexOf("rows=""0""") >= 0 Then
                    inUltra = 0
                    thisTable.Rows(i).Item(3) = ultraState
                    thisTable.Rows(i).Item(4) = inUltra
                ElseIf response.IndexOf("rows=") >= 0 Then
                    ultraState = GetValueFromXML(response, "_State")
                End If

                If inUltra = 0 Or ultraState <> dbState Then
                    Select Case dbState
                        Case 0
                            ultraState = 0
                        Case 2
                            ultraState = 0
                        Case Else
                            ultraState = 4
                    End Select

                    returnTable.Rows.Add(listID, rowID, dbState, ultraState, inUltra)

                    If inUltra = 1 Then
                        request = "<upload clientid='10864'><" & schema & " BaseIndexId='" & listID & "'><rows>"
                        request = request & "<updateOnly RowId='" & rowID.ToString & "' _State='" & ultraState & "' /></rows></" & schema & "></upload>"
                        'If i Mod 50 = 0 Then
                        '    MsgBox("<= UPDATE DATA REQUEST => " & request)
                        'End If

                        response = GetUltraUpload(request, un, pw)
                        'If i Mod 50 = 0 Then
                        '    MsgBox("<= UPDATE DATA RESPONSE - ListID: " & listID.ToString & " - RowID: " & rowID.ToString & " => " & response)
                        'End If
                    End If
                End If

                If i = rowsToRun - 1 Then
                    If returnTable.Rows.Count > 0 Then
                        Call ReturnStateDataToDB(returnTable)
                    End If

                    returnTable.Clear()

                    If rows - 1 - i < rowsPerRun Then
                        rowsToRun = rows - 1 - i
                    Else
                        rowsToRun += rowsPerRun
                    End If
                End If
            Next i
        Catch
        End Try

        'MsgBox("acounts check complete & took " & DateDiff(DateInterval.Second, thisStart, Now()).ToString & " seconds")
    End Sub

    Sub ReturnStateDataToDB(ByVal returnTable As DataTable)
        Try
            Dim newRow As Integer = returnTable.Rows.Count
            Dim queryBreak As Integer = 499
            Dim rowsAdd As Integer = 500
            Dim startQuery As String
            Dim endQuery As String
            Dim query As String
            Dim sendNow As Integer = 0

            'MsgBox("returnTable -> rows: " & newRow.ToString)
            startQuery = "declare @comp table ( ListID int, RowID int, DB_State int, UltraState int, InUltra int ) " & Environment.NewLine
            query = Environment.NewLine & startQuery

            endQuery = Environment.NewLine
            endQuery = endQuery & "update c set DB_State = p.DB_State, UltraState = p.UltraState, InUltra = p.InUltra, DateChecked = getdate() "
            endQuery = endQuery & Environment.NewLine
            endQuery = endQuery & "from DiallerStagingArea.dbo.CheckAccountStateComparison c " & Environment.NewLine
            endQuery = endQuery & " join @comp p on c.ListID = p.ListID And c.RowID = p.RowID " & Environment.NewLine & Environment.NewLine

            endQuery = endQuery & "update a set " & Environment.NewLine
            endQuery = endQuery & " StagingAreaStatus = case when c.DB_State != a.StagingAreaStatus then c.DB_State else a.StagingAreaStatus end "
            endQuery = endQuery & Environment.NewLine
            endQuery = endQuery & " , DiallerStatus = case when c.UltraState != a.DiallerStatus then c.UltraState else a.DiallerStatus end "
            endQuery = endQuery & Environment.NewLine
            endQuery = endQuery & " , DateModified = getdate() " & Environment.NewLine
            endQuery = endQuery & "from DiallerStagingArea.dbo.CheckAccountStateComparison c " & Environment.NewLine
            endQuery = endQuery & " join DiallerStagingArea.dbo.CurrentAccounts a on c.ListID = a.ListID and c.RowID = a.RowID " & Environment.NewLine
            endQuery = endQuery & "     and (c.DB_State != a.StagingAreaStatus or c.UltraState != a.DiallerStatus) " & Environment.NewLine
            endQuery = endQuery & " join @comp p on a.ListID = p.ListID And a.RowID = p.RowID " & Environment.NewLine
            endQuery = endQuery & "where c.InDB =1 " & Environment.NewLine

            'ListID (0), RowID (1), DB_State (2), UltraState (3), InUltra (4)
            For i As Integer = 0 To newRow - 1
                If returnTable.Rows(i).Item(2) = 0 Then returnTable.Rows(i).Item(2) = 2

                If i <= queryBreak Then
                    'add to sql request
                    query = query & "insert into @comp Select "
                    query = query & returnTable.Rows(i).Item(0).ToString & ", " & returnTable.Rows(i).Item(1).ToString
                    query = query & ", " & returnTable.Rows(i).Item(2).ToString & ", " & returnTable.Rows(i).Item(3).ToString
                    query = query & ", " & returnTable.Rows(i).Item(4).ToString & " " & Environment.NewLine
                Else
                    query = query & endQuery
                    sendNow = 1

                    'start new query
                    query = startQuery & Environment.NewLine & "insert into @comp Select "
                    query = query & returnTable.Rows(i).Item(0).ToString & ", " & returnTable.Rows(i).Item(1).ToString
                    query = query & ", " & returnTable.Rows(i).Item(2).ToString & ", " & returnTable.Rows(i).Item(3).ToString
                    query = query & ", " & returnTable.Rows(i).Item(4).ToString & " " & Environment.NewLine

                    'increase queryBreak
                    If newRow + 1 - i < rowsAdd Then
                        queryBreak = newRow
                    Else
                        queryBreak += rowsAdd
                    End If
                End If

                If i = newRow - 1 Then
                    query = query & endQuery
                    sendNow = 1
                End If

                'send sql request
                If sendNow = 1 Then
                    'MsgBox("<= UPDATE DB QUERY => " & query)
                    Call ConnectToSql(query, False)
                    sendNow = 0
                End If
            Next i
        Catch
        End Try
    End Sub

    Sub CheckAccountStates()
        Try
            Dim schema As String = ""
            Dim listID As String = ""
            Dim rowID As String = ""
            Dim loanNo As String = ""
            Dim batchID As String = ""
            Dim ultraState As String = ""
            Dim claimID As String = ""
            Dim schedule As Date = Now
            Dim scheduled As String = ""
            Dim schedulePriority As String = ""
            Dim scheduleType As String = ""
            Dim query As String = ""
            Dim request As String = ""
            Dim returnVal As String = ""
            Dim table As New DataTable
            Dim dt As New DataTable
            Dim listsTable As New DataTable
            Dim rowString As String = ""
            Dim thisColVal As String = ""
            Dim thisListID As String = ""
            Dim thisTable As DataTable
            Dim criteria() As String
            Dim thisListString As String = ""
            Dim i As Integer = 0
            Dim results As String = ""
            Dim sSchema As String = ""
            Dim sURL As String = "http://client.ultraasp.net/ultra.client.upload/service.asmx"
            Dim soapResponse As String = ""
            Dim timeStamp As String = ""
            Dim timesToTry As Integer = 3
            Dim timesTried As Integer = 0

            table = sqlToDataTable("exec DiallerStagingArea.dbo.sp_AccountsToCompare_Get ")
            Dim cols As Integer = table.Columns.Count - 1
            Dim rows As Integer = table.Rows.Count - 1
            Dim data(rows, cols) As String

            'MsgBox("Getting data from Ultra to compare; rows: " & rows.ToString)
            If rows > 0 Then
                For i = 0 To rows
                    With table
                        loanNo = .Rows(i)(0)
                        listID = .Rows(i)(1)
                        rowID = .Rows(i)(2)
                        batchID = .Rows(i)(3)
                    End With

                    If rowID <> "0" Then
                        returnVal = "error="
                        timesTried = 0

                        While returnVal.IndexOf("error=") >= 0
                            schema = GetSchema(listID)
                            request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID & "' RowId='" & rowID & "'/></data>"
                            returnVal = UltraDownload(request)

                            timesTried += 1
                            If timesTried >= timesToTry Then Exit While
                        End While

                        If timesTried <= timesToTry And returnVal.IndexOf("error=") < 0 Then
                            ultraState = GetValueFromXML(returnVal, "_State")
                            claimID = GetValueFromXML(returnVal, "_ClaimId")
                            schedule = GetValueFromXML(returnVal, "_Schedule")
                            scheduled = GetValueFromXML(returnVal, "_Scheduled")
                            schedulePriority = GetValueFromXML(returnVal, "_SchedulePriority")
                            scheduleType = GetValueFromXML(returnVal, "_ScheduleType")

                            query = "exec DiallerStagingArea.dbo.sp_AccountsToCompare_Return @loanNo = " & loanNo & " , @listID = " & listID & " , @rowID = " & rowID & " , @batchID = " _
                                    & batchID & " , @ultraState = " & ultraState & " , @claimID = '" & claimID.ToString & "' , @schedule = '" & schedule & "' , @scheduled = '" _
                                    & scheduled.ToString & "' , @schedulePriority = '" & schedulePriority.ToString & "' , @scheduleType = '" & scheduleType.ToString & "' "
                            Call ConnectToSql(query)
                        End If
                    End If
                Next i

                Dim thisBox As TextBox = ReturnStartBox()
                Dim startTime As String = GetNumberFromBox(thisBox.Text.ToString, 1).ToString & ":" & GetNumberFromBox(thisBox.Text.ToString, 0).ToString & ":00"


                query = "exec DiallerStagingArea.dbo.sp_AccountsToCompare_Differ @batchID = " & batchID.ToString & ", @shiftStart = '" & startTime.ToString & "' "
                dt = sqlToDataTable(query)
                listsTable = dt.DefaultView.ToTable(True, "ListID")

                ReDim criteria(1)
                criteria(0) = "UpdateState"
                criteria(1) = "UpdateSchedule"

                For Each row As DataRow In listsTable.Rows
                    thisListID = row.Item("ListID")

                    For i = 0 To criteria.Length - 1
                        thisTable = FilterDataTable(dt, thisListID, criteria(i), criteria)

                        If thisTable.Rows.Count > 0 Then
                            thisListString = XMLfromDataTable(thisTable, criteria(i))
                            sSchema = GetSchema(thisListID)

                            If Len(sSchema) > 0 Then
                                thisListString = buildEnvelope(thisListString, sSchema, thisListID)
                                batchID = GetBatchID(thisListID, dt, criteria(i))

                                timeStamp = Now.ToString("yyyyMMdd_HHmmss")
                                query = "exec DiallerStagingArea.dbo.sp_StoreUltraRequests @batchID = '" & batchID.ToString & "' , @listID = '" & thisListID.ToString &
                                "' , @RequestXML = '" & thisListString & "' , @timeStamp ='" & timeStamp.ToString & "'"
                                Call ConnectToSql(query, False)

                                soapResponse = sendSoap(sURL, thisListString, thisListID)
                                soapResponse = Replace(soapResponse, "rows count=", "rows batchID=""" & batchID & """ count=")

                                query = "exec DiallerStagingArea.dbo.sp_StoreUltraResponses @batchID = '" & batchID.ToString & "' , @listID = '" & thisListID.ToString &
                                "' , @ResponseXML = '" & soapResponse & "' , @timeStamp ='" & timeStamp.ToString & "'"
                                Call ConnectToSql(query, False)
                            End If
                        End If
                    Next i
                Next row

            End If

            query = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) " &
                " select 'Check Account State & Schedule Complete' "
            Call ConnectToSql(query, False)
        Catch
        End Try
    End Sub
End Module
