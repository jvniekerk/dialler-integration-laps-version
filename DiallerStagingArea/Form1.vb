﻿Option Explicit On
Imports System.ComponentModel

'error releasing app
'https://social.msdn.microsoft.com/Forums/vstudio/en-US/dafcfdde-b238-410b-8c82-4f4afcb9aee5/vs2015-error-8007006e-unable-to-finish-updating-resource-for-binreleaseapppublishsetupexe?forum=visualstudiogeneral

Public Class Form1
    Inherits System.Windows.Forms.Form

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        For Each ctrl As Control In Controls
            If TypeOf ctrl Is Button Then
                Dim btn As Button = ctrl
                btn.FlatStyle = FlatStyle.Flat
                btn.FlatAppearance.MouseOverBackColor = Color.Aqua
            End If
        Next ctrl

        lblCurrDate.Text = Now.ToString
        Application.DoEvents()

        'Try
        '    Call bgwCallAttempts.RunWorkerAsync()
        'Catch ex As Exception
        'End Try
    End Sub

    Private Sub btnRun_Click(sender As Object, e As EventArgs) Handles btnRun.Click
        Call ResetTimer()
        With tmrElapsedTime
            .Enabled = True
            .Start()
        End With

        Try
            Call BackgroundDiallerUpdate()
        Catch
        End Try
    End Sub

    Private Sub btnExit_Click(sender As Object, e As EventArgs) Handles btnExit.Click
        If bgwUpdateDialler.IsBusy Then
            MsgBox("Please STOP the Dialler Update operation before exiting!!", vbOKOnly + vbCritical, "Dialler updating still in progress")
        Else
            If MsgBox("Are you sure you want To Exit?", vbOKCancel + vbInformation, "Exit Staging Area") = 1 Then Close()
        End If
    End Sub

    Private Sub btnAddUser_Click(sender As Object, e As EventArgs) Handles btnAddUser.Click
        frmManageAgents.Show()
        Hide()
    End Sub

    Private Sub btnRebuildTables_Click(sender As Object, e As EventArgs) Handles btnRebuildTables.Click
        'If MsgBox("Are you sure you want to rebuild the DB tables?", vbOKCancel + vbQuestion, "Rebuild Database Tables") = 1 Then
        '    If MsgBox("ARE YOU ABSOLUTELY SURE???", vbOKCancel + vbExclamation, "Rebuild Database Tables") = 1 Then
        '        Call ConnectToSql("exec DiallerStagingArea.dbo.sp_RebuildTables", False)
        '        MsgBox("Done rebuilding database tables. Please remember to delete all data from Ultra lists before restarting the dialler update.")
        '    End If
        'End If
        MsgBox("This functionality has been deactivated!!", vbOKOnly + vbExclamation, "Rebuild Database Tables")
    End Sub

    Private Sub btnStop_Click(sender As Object, e As EventArgs) Handles btnStop.Click
        bgwCallAttempts.CancelAsync()

        If MsgBox("Cancel button clicked - Please WAIT for thread to finish before exiting!!", vbOKCancel + vbInformation, "Dialler update") = 1 Then
            btnStop.Enabled = False
            Call bgwUpdateDialler.CancelAsync()
            Call bgwCallAttempts.CancelAsync()
        End If
    End Sub

    Private Sub BackgroundDiallerUpdate()
        btnStop.Enabled = True
        btnRun.Enabled = False

        Try
            Call bgwCallAttempts.RunWorkerAsync()
            Call bgwUpdateDialler.RunWorkerAsync()
        Catch
        End Try
    End Sub

    Private Sub bgwUpdateDialler_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwUpdateDialler.DoWork
        Try
            Dim worker As BackgroundWorker = CType(sender, BackgroundWorker)
            Dim timeOK As Boolean = CheckTime()
            Dim cancelOK As Boolean = CheckCancel(worker)
            'MsgBox("starting")

            Dim query As String = "select convert(varchar(10), count(distinct u.RunID)) as TimesRun from DiallerStagingArea.dbo.UpdateStoredProceduresRun u "
            query += "where u.DateRun >= convert(date, getdate()) and u.ProcedureRun = 'Max Attempts Reset'"
            'Dim MaxReset As Integer = ConnectToSql(query, False)
            'Dim lastChecked As Date = DateAdd(DateInterval.Day, -1, Today())
            Dim thisQuery As String = "exec [DiallerStagingArea].[dbo].[sp_AccountsToCompare_CheckToRun]"
            'Dim run As Integer = ConnectToSql(thisQuery, False)

            While Now < DateAdd(DateInterval.Month, 1, Now) And e.Cancel = False
                cancelOK = CheckCancel(worker)
                timeOK = CheckTime()
                If (timeOK And cancelOK) Then
                    'If lastChecked < Today() Then
                    '    lastChecked = Now()
                    '    MaxReset = ConnectToSql(query, False)

                    '    If MaxReset = 0 Then
                    '        'Call ResetMaxAttempts()
                    '        'Call getLists()
                    '        MaxReset = 1
                    '    End If
                    'End If

                    'MsgBox("times ok")

                    Call updateDialler(worker, e)
                ElseIf ConnectToSql(thisQuery, False) = 1 Then
                    Try
                        Call bgwAccountStateCheck.RunWorkerAsync()
                    Catch
                    End Try
                End If

                If Not cancelOK Then
                    e.Cancel = True
                    MsgBox("Upload Thread Complete. You can Exit now Or restart the update.")
                ElseIf Not timeOK Then
                    Threading.Thread.Sleep(60 * 1000 * 10)
                End If
            End While
        Catch
        End Try
    End Sub

    Private Sub bgwAccountStateCheck_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwAccountStateCheck.DoWork
        Try
            Call RunAccountStateCheck()
        Catch
        End Try
    End Sub

    Private Sub bgwCallAttempts_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwCallAttempts.DoWork
        Try
            While Now() < DateAdd(DateInterval.Month, 1, Now())
                Dim startTime As DateTime = Today & " " & txtDownloadStart.Text.ToString
                Dim endTime As DateTime = Today & " " & txtDownloadEnd.Text.ToString
                Dim thisNow As DateTime = Now()

                'Dim outcome As String
                'If Now > startTime And Now < endTime Then
                '    outcome = "in"
                'Else
                '    outcome = "out"
                'End If
                'MsgBox("start='" & startTime.ToString & "' end='" & endTime.ToString & "' outcome=" & outcome)

                While thisNow > startTime And thisNow < endTime
                    'MsgBox("start download")
                    Try
                        Call CallAttemptsDownloadByID()
                    Catch
                    End Try

                    Try
                        Call DispositionsDownloadByID()
                    Catch
                    End Try

                    Try
                        Call UpdateAgentStates()
                    Catch
                    End Try

                    'Call CallAttemptsToLapsNotes()     'no longer added to LAPS
                    'Call GetUserStates()               'now run with UpdateAgentStates()
                    'Call UpdateUsers()                 'now run with UpdateAgentStates()
                    'Call getLists()                    'now run with UpdateAgentStates()

                    startTime = Today & " " & txtDownloadStart.Text.ToString
                    endTime = Today & " " & txtDownloadEnd.Text.ToString
                    thisNow = Now()

                    'MsgBox("end download")
                End While

                If Now > endTime Or Now < startTime Then
                    Dim diff As Integer = DateDiff(DateInterval.Minute, Now, startTime.AddDays(1))
                    Dim sleep As Integer = Math.Ceiling(diff / 10)
                    'MsgBox("going to sleep=" & sleep)
                    If sleep >= 1 Then Threading.Thread.Sleep(sleep)
                End If
            End While
        Catch
        End Try
    End Sub

    Private Sub bgwCollections_DoWork(sender As Object, e As DoWorkEventArgs) Handles bgwCollections.DoWork
        Try
            Call CallAttemptsToLapsNotes()
        Catch
        End Try
    End Sub

    Private Sub bgwCallAttempts_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwCallAttempts.RunWorkerCompleted

    End Sub

    Private Sub bgwUpdateDialler_RunWorkerCompleted(sender As Object, e As RunWorkerCompletedEventArgs) Handles bgwUpdateDialler.RunWorkerCompleted
        btnStop.Enabled = False
        btnRun.Enabled = True

        With tmrElapsedTime
            .Stop()
            .Enabled = False
        End With
    End Sub

    Function CheckTime() As Boolean
        Dim startHour As String
        Dim startMinute As String = 0
        Dim endHour As String
        Dim endMinute As String = 0
        Dim todayOK As Boolean = False
        Dim timeOK As Boolean = False
        Dim thisStartBox As TextBox
        Dim thisEndBox As TextBox
        Dim thisDayBox As CheckBox
        Dim thisStart As Date = Today
        Dim thisEnd As Date = Today

        With Me
            Select Case Now.ToString("dddd")
                Case "Monday"
                    thisDayBox = .chkMonday
                    thisStartBox = .txtMonStart
                    thisEndBox = .txtMonEnd
                Case "Tuesday"
                    thisDayBox = .chkTuesday
                    thisStartBox = .txtTueStart
                    thisEndBox = .txtTueEnd
                Case "Wednesday"
                    thisDayBox = .chkWednesday
                    thisStartBox = .txtWedStart
                    thisEndBox = .txtWedEnd
                Case "Thursday"
                    thisDayBox = .chkThursday
                    thisStartBox = .txtThuStart
                    thisEndBox = .txtThuEnd
                Case "Friday"
                    thisDayBox = .chkFriday
                    thisStartBox = .txtFriStart
                    thisEndBox = .txtFriEnd
                Case "Saturday"
                    thisDayBox = .chkSaturday
                    thisStartBox = .txtSatStart
                    thisEndBox = .txtSatEnd
                Case Else
                    thisDayBox = .chkSunday
                    thisStartBox = .txtSunStart
                    thisEndBox = .txtSunEnd
            End Select
        End With

        If thisDayBox.Checked = True Then todayOK = True

        With thisStartBox.Text
            startHour = GetNumberFromBox(.ToString, 1)
            startMinute = GetNumberFromBox(.ToString, 0)
        End With
        With thisEndBox.Text
            endHour = GetNumberFromBox(.ToString, 1)
            endMinute = GetNumberFromBox(.ToString, 0)
        End With

        ''check if today is the last working day or last working friday and add an hour to start and 2 hours to end hours
        If isLastWorkingDay(Today) Or isLastWorkingFriday(Today) Then
            startHour -= 1
            endHour += 2
        End If

        If startHour.ToString <> "Invalid" And startMinute.ToString <> "Invalid" _
                And endHour.ToString <> "Invalid" And endMinute.ToString <> "Invalid" Then
            thisStart = thisStart & " " & startHour.ToString & ":" & startMinute.ToString
            thisStart = DateAdd(DateInterval.Minute, -90, thisStart)
            thisEnd = thisEnd & " " & endHour.ToString & ":" & endMinute.ToString
        End If

        If Now >= thisStart And Now < thisEnd Then timeOK = True

        Dim returnResult As Boolean
        If timeOK And todayOK Then
            returnResult = True
        Else
            returnResult = False
        End If
        'MsgBox("checkTime: " & returnResult.ToString)

        Return returnResult
    End Function

    Sub ResetTimer()
        lblProgress.Text = "Dialler Update Time Lapsed: " & 0 & " minutes " & 0 & " seconds"
    End Sub

    Sub ProgressTimerUpdate()
        Dim thisText As String = lblProgress.Text.ToString
        thisText = thisText.Replace("Dialler Update Time Lapsed: ", "")
        thisText = thisText.Replace(" seconds", "")
        Dim mins As Integer = Microsoft.VisualBasic.Left(thisText, thisText.IndexOf(" minutes"))
        Dim secs As Integer = thisText.Replace(mins.ToString & " minutes ", "") + 1

        If secs > 59 Then
            mins += 1
            secs -= 60
        End If

        lblProgress.Text = "Dialler Update Time Lapsed: " & mins & " minutes " & secs & " seconds"
    End Sub

    Private Sub tmrElapsedTime_Tick(sender As Object, e As EventArgs) Handles tmrElapsedTime.Tick
        Try
            Call ProgressTimerUpdate()
        Catch
        End Try
    End Sub

    Private Sub tmrCurrDate_Tick(sender As Object, e As EventArgs) Handles tmrCurrDate.Tick
        Try
            Call CurrDateTimerUpdate()
        Catch
        End Try
    End Sub

    Sub CurrDateTimerUpdate()
        lblCurrDate.Text = Now.ToString
    End Sub

    Private Sub btnLists_Click(sender As Object, e As EventArgs) Handles btnLists.Click
        If MsgBox("Are you sure you want to rebuild the Lists table?", vbOKCancel + vbQuestion, "Rebuild Lists Table") = 1 Then
            Call getLists()
            MsgBox("Done rebuilding the Lists table.")
        End If
    End Sub

    Public Sub updateListBox(toAdd As String, thisBox As String)
        Try
            'MsgBox(toAdd & " => " & thisBox.Name.ToString)
            Dim runThis As Boolean = False
            Dim theBox As ListBox '= lsbDownloadProgress

            If thisBox = "lsbDownloadProgress" Then
                theBox = lsbDownloadProgress
                runThis = True
                'ElseIf thisBox = "lsbUpdateProgress" Then
            Else
                theBox = lsbUpdateProgress
                runThis = True
            End If

            If runThis Then
                With theBox
                    .Items.Add(toAdd)
                    If .Items.Count() >= 38 Then .Items.RemoveAt(0)
                    .Update()
                    .Refresh()
                End With
            End If
        Catch
        End Try
    End Sub

    Public Function testCheckTime(thisDate As DateTime) As Boolean
        Dim startHour As String
        Dim startMinute As String = 0
        Dim endHour As String
        Dim endMinute As String = 0
        Dim todayOK As Boolean = False
        Dim timeOK As Boolean = False
        Dim thisStartBox As TextBox
        Dim thisEndBox As TextBox
        Dim thisDayBox As CheckBox
        Dim thisStart As Date = Today
        Dim thisEnd As Date = Today

        With Me
            Select Case Now.ToString("dddd")
                Case "Monday"
                    thisDayBox = .chkMonday
                    thisStartBox = .txtMonStart
                    thisEndBox = .txtMonEnd
                Case "Tuesday"
                    thisDayBox = .chkTuesday
                    thisStartBox = .txtTueStart
                    thisEndBox = .txtTueEnd
                Case "Wednesday"
                    thisDayBox = .chkWednesday
                    thisStartBox = .txtWedStart
                    thisEndBox = .txtWedEnd
                Case "Thursday"
                    thisDayBox = .chkThursday
                    thisStartBox = .txtThuStart
                    thisEndBox = .txtThuEnd
                Case "Friday"
                    thisDayBox = .chkFriday
                    thisStartBox = .txtFriStart
                    thisEndBox = .txtFriEnd
                Case "Saturday"
                    thisDayBox = .chkSaturday
                    thisStartBox = .txtSatStart
                    thisEndBox = .txtSatEnd
                Case Else
                    thisDayBox = .chkSunday
                    thisStartBox = .txtSunStart
                    thisEndBox = .txtSunEnd
            End Select
        End With

        If thisDayBox.Checked = True Then todayOK = True

        With thisStartBox.Text
            startHour = GetNumberFromBox(.ToString, 1)
            startMinute = GetNumberFromBox(.ToString, 0)
        End With
        With thisEndBox.Text
            endHour = GetNumberFromBox(.ToString, 1)
            endMinute = GetNumberFromBox(.ToString, 0)
        End With

        ''check if today is the last working day or last working friday and add an hour to start and 2 hours to end hours
        If isLastWorkingDay(Today) Or isLastWorkingFriday(Today) Then
            startHour -= 1
            endHour += 2
        End If

        If startHour.ToString <> "Invalid" And startMinute.ToString <> "Invalid" _
                And endHour.ToString <> "Invalid" And endMinute.ToString <> "Invalid" Then
            thisStart = thisStart & " " & startHour.ToString & ":" & startMinute.ToString
            thisStart = DateAdd(DateInterval.Minute, -150, thisStart)
            thisEnd = thisEnd & " " & endHour.ToString & ":" & endMinute.ToString
            thisEnd = DateAdd(DateInterval.Minute, 15, thisEnd)
        End If

        If Now >= thisStart And Now < thisEnd Then timeOK = True

        Dim returnResult As Boolean
        If timeOK And todayOK Then
            returnResult = True
        Else
            returnResult = False
        End If
        'MsgBox("checkTime: " & returnResult.ToString)

        Return returnResult
    End Function

    Private Sub cmbTest_Click(sender As Object, e As EventArgs) Handles cmbTest.Click
        Dim pword As String = "test"
        cmbTest.Enabled = False
        If InputBox("Please enter a password to continue", "Password", "Please enter a password") = pword Then
            'If InputBox("Please enter a password to continue", "Password", "test") = pword Then


            'https://social.msdn.microsoft.com/Forums/vstudio/en-US/dafcfdde-b238-410b-8c82-4f4afcb9aee5/vs2015-error-8007006e-unable-to-finish-updating-resource-for-binreleaseapppublishsetupexe?forum=visualstudiogeneral



            Dim thisDate As DateTime = New DateTime(2018, 9, 16, 11, 0, 0)
            'MsgBox(CheckTime())
            MsgBox("thisDate: " & thisDate.ToString & " -> CheckDate: " & testCheckTime(thisDate).ToString)






            'Dim sleeper As Integer = 300
            ''Dim i As Integer
            ''For i = 1 To 20
            ''    With lsbDownloadProgress
            ''        .Items.Add("row " & i.ToString)
            ''        .Refresh()
            ''    End With
            ''    Threading.Thread.Sleep(sleeper)
            ''Next

            ''For t As Integer = i + 1 To 60
            ''    With lsbDownloadProgress
            ''        .Items.Add("row " & t.ToString)
            ''        If .Items.Count() >= 38 Then .Items.RemoveAt(0)
            ''        .Refresh()
            ''    End With
            ''    Threading.Thread.Sleep(sleeper)
            ''Next



            'For t As Integer = 1 To 60
            '    Call updateListBox("row " & t, "lsbUpdateProgress")
            '    Threading.Thread.Sleep(sleeper)
            '    'anothertest("row " & t)
            'Next


            ''MsgBox(Now > Today & " " & txtDownloadStart.Text.ToString And Now < Today & " " & txtDownloadEnd.Text.ToString)
            ''MsgBox(txtDownloadEnd.Text.ToString)
            ''MsgBox(txtDownloadEnd.Text.ToString < TimeOfDay)


            ''Dim thisDay As Date = Today
            ''Dim startHour As String = 9
            ''Dim endHour As String = 18
            ''If isLastWorkingDay(thisDay) Or isLastWorkingFriday(thisDay) Then
            ''    startHour -= 1
            ''    endHour += 2
            ''End If
            ''MsgBox(thisDay & " last start=" & startHour & " end=" & endHour)

            ''thisDay = DateAdd(DateInterval.Day, 1, Today)
            ''startHour = 9
            ''endHour = 18
            ''If isLastWorkingDay(thisDay) Or isLastWorkingFriday(thisDay) Then
            ''    startHour -= 1
            ''    endHour += 2
            ''End If
            ''MsgBox(thisDay & " last start=" & startHour & " end=" & endHour)

            ''thisDay = "30/7/2018"
            ''If isLastWorkingDay(thisDay) Or isLastWorkingFriday(thisDay) Then
            ''    MsgBox(thisDay & " last")
            ''Else
            ''    MsgBox(thisDay & " not last")
            ''End If
            ''thisDay = "31/7/2018"
            ''If isLastWorkingDay(thisDay) Or isLastWorkingFriday(thisDay) Then
            ''    MsgBox(thisDay & " last")
            ''Else
            ''    MsgBox(thisDay & " not last")
            ''End If
















            '''' list bank holidays for next 3 years
            'Dim thisDate As Date = Today
            'MsgBox(thisDate & " n => " & checkHoliday(thisDate))
            'thisDate = "27/8/2018"
            'MsgBox(thisDate & " y => " & checkHoliday(thisDate))
            'thisDate = "25/9/2018"
            'MsgBox(thisDate & " n => " & checkHoliday(thisDate))
            'thisDate = "27/05/2019"
            'MsgBox(thisDate & " y => " & checkHoliday(thisDate))
            'thisDate = "31/08/2020"
            'MsgBox(thisDate & " y => " & checkHoliday(thisDate))
            'thisDate = "23/1/2021"
            'MsgBox(thisDate & " n => " & checkHoliday(thisDate))
            'thisDate = "26/12/2021"
            'MsgBox(thisDate & " y => " & checkHoliday(thisDate))



            '''' see if today is last working day of the month
            'Dim thisDate As Date = Today
            'MsgBox(thisDate & " n => " & isLastWorkingDay(thisDate))
            'thisDate = "30/7/2018"
            'MsgBox(thisDate & " n => " & isLastWorkingDay(thisDate))
            'thisDate = "31/7/2018"
            'MsgBox(thisDate & " y => " & isLastWorkingDay(thisDate))
            'thisDate = "29/05/2020"
            'MsgBox(thisDate & " y => " & isLastWorkingDay(thisDate))
            'thisDate = "28/09/2018"
            'MsgBox(thisDate & " y => " & isLastWorkingDay(thisDate))
            'thisDate = "30/9/2018"
            'MsgBox(thisDate & " n => " & isLastWorkingDay(thisDate))
            'thisDate = "31/12/2021"
            'MsgBox(thisDate & " y => " & isLastWorkingDay(thisDate))

            ''Dim thisYear As Integer = Today.Year
            ''Dim thisMonth As Integer = Today.Month
            ''Dim d As Integer = Date.DaysInMonth(thisYear, thisMonth)
            ''MsgBox("year:" & thisYear & " month:" & thisMonth & " day:" & d)
            ''thisDate = New Date(thisYear, thisMonth, 23)
            ''MsgBox(thisDate)
            ''thisDate = New Date(thisYear, thisMonth, d)
            ''MsgBox(thisDate)
            'thisDate = getLastWorkingDay(Today.Year, Today.Month)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingDay(Today.Year, 6)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingDay(Today.Year, 3)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingDay(2021, 5)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingDay(2020, 12)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))


            ''''' get the last working friday of the month
            'Dim thisDate As Date = Today
            'thisDate = Today
            'MsgBox(thisDate & " n => " & isLastWorkingFriday(thisDate))
            'thisDate = "27/7/2018"
            'MsgBox(thisDate & " y => " & isLastWorkingFriday(thisDate))
            'thisDate = "31/7/2018"
            'MsgBox(thisDate & " n => " & isLastWorkingFriday(thisDate))
            'thisDate = "29/04/2019"
            'MsgBox(thisDate & " n => " & isLastWorkingFriday(thisDate))
            'thisDate = "26/04/2019"
            'MsgBox(thisDate & " y => " & isLastWorkingFriday(thisDate))

            ''Dim thisYear As Integer = Today.Year
            ''Dim thisMonth As Integer = Today.Month
            ''Dim d As Integer = Date.DaysInMonth(thisYear, thisMonth)
            ''MsgBox("year:" & thisYear & " month:" & thisMonth & " day:" & d)
            ''thisDate = New Date(thisYear, thisMonth, 23)
            ''MsgBox(thisDate)
            ''thisDate = New Date(thisYear, thisMonth, d)
            ''MsgBox(thisDate)
            'thisDate = getLastWorkingFriday(Today.Year, Today.Month)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingFriday(Today.Year, 6)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingFriday(Today.Year, 3)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingFriday(2021, 5)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))
            'thisDate = getLastWorkingFriday(2020, 12)
            'MsgBox(thisDate & " " & thisDate.ToString("dddd"))





            'Dim test As Integer = 1
            ''Dim startTime As String = "05:00:00"
            ''Dim endTime As String = "21:00:00"
            'Dim startTime As DateTime = Today & " 05:00:00"
            'Dim endTime As DateTime = Today & " 21:00:00"

            ''While Now > startTime And Now < endTime
            ''    MsgBox("after 5 and before 21 -> i:" & test)

            ''    test += 1

            ''    If test >= 10 Then
            ''        Exit While
            ''    End If
            ''End While

            ''Dim diff As Integer = DateDiff(DateInterval.Minute, Now, endTime) / 10
            ''MsgBox(DateDiff(DateInterval.Minute, Now, endTime) & " " & diff)
            ''endTime = Today & " 13:00:00"

            'If Now > endTime Or Now < startTime Then
            '    Dim diff As Integer = DateDiff(DateInterval.Minute, Now, startTime.AddDays(1))
            '    Dim sleep As Integer = Math.Floor(diff / 10)
            '    'If sleep > 1 Then MsgBox("diff: " & diff & " sleeping for " & sleep & " minutes")
            '    If sleep > 1 Then Threading.Thread.Sleep(sleep)
            'End If



            'MsgBox("starting")
            'Dim start As DateTime = Now
            'Dim minMins As Integer = 0.5 * 60   '5 * 60
            'Threading.Thread.Sleep(10000)
            'Dim span As TimeSpan = Now - start
            'Dim duration As Double = span.TotalSeconds
            'MsgBox("start: " & start & " -> now: " & Now & " -> duration: " & duration)
            'Dim toWait As Double = (minMins - duration) * 1000

            'If toWait > 0 Then
            '    MsgBox("toWait: " & toWait / 1000 & " seconds")
            '    Threading.Thread.Sleep(toWait)
            'End If




            '''''Call DeleteOldAccounts()
            'Dim un As String
            'Dim pw As String
            'Dim schema As String = "StagingAreaIntegration"
            'Dim request As String
            'Dim response As String
            ''Dim loans As DataTable
            'Dim query As String
            'Dim listID As Integer
            'Dim rowID As Integer
            'Dim rows As Integer
            'Dim loanNo As Integer

            'un = "Int_Test"
            'pw = "Unc13Buck"


            'Call UpdateUsers()
            'Call CallAttemptsDownloadByID()
            'Call CallAttemptsToLapsNotes()



            ''Check call attempts from Ultra
            'Dim id As String = ConnectToSql("exec DiallerStagingArea.[dbo].[sp_CallAttempts_LastCDR_ID] ", False)
            'request = "<data clientid='10864'><Report.CustomerCDRId ID='" & id.ToString & "' RowCount='5' /></data>"
            'response = GetUltraDownload(request, un, pw)
            'MsgBox(response)

            ''update call attempts onto dialler staging aread db
            'Dim rows As Integer = getRowNumbers(response)
            'If rows > 0 Then
            '    Dim query = "exec DiallerStagingArea.[dbo].[sp_DownloadDataStoreXML] 'CDR' , '" & response & "' "
            '    Call ConnectToSql(query, False)
            'End If







            'request = "<data clientid='10864'><Framework.User /></data>"
            'response = GetUltraDownload(request, un, pw)
            ''Call saveTextFile("C:\Users\jvniekerk.UNCLEBUCK\Desktop\user_test.txt", response)
            'MsgBox(response)
            ''query = "exec exec DiallerStagingArea.dbo.sp_Update_UserStates @xml = '" & response & "' "
            ''Call ConnectToSql(query, False)








            ''Call GetUserStates()

            ''Dim i As Integer = 1
            ''While i <= 100
            ''    Call UpdateAgentStates()
            ''    i = i + 1
            ''End While










            ''''query = "select distinct c.ListID, c.RowID, c.LoanNo from DiallerStagingArea.dbo.CurrentAccounts c "
            ''''query += "where c.DateModified < convert(date, dateadd(week, -1, getdate())) and c.StagingAreaStatus =4 "
            ''''loans = sqlToDataTable(query)
            ''''rows = loans.Rows.Count

            ''''If rows > 0 Then
            ''''    For i As Integer = 0 To 0   'rows - 1
            ''''        listID = loans.Rows(i).Item(0)
            ''''        rowID = loans.Rows(i).Item(1)
            ''''        loanNo = loans.Rows(i).Item(2)

            ''''        request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID.ToString & "' RowId='" & rowID.ToString & "'/></data>"
            ''''        response = GetUltraDownload(request, un, pw)
            ''''        'MsgBox(response)

            ''''        If response.IndexOf("rows=""") > 0 Then
            ''''            request = "<upload clientid='10864'><System.Base.Counters BaseIndexId='" & listID & "'><rows>"
            ''''            request += "<DeleteOnly _RowId='" & rowID & "'/></rows></System.Base.Counters></upload>"
            ''''            response = GetUltraUpload(request, un, pw)
            ''''            'MsgBox(response)

            ''''            If response.IndexOf("outcome=""ok") > 0 Then
            ''''                query = "delete from DiallerStagingArea.dbo.CurrentAccounts where ListID = " & listID.ToString & " and RowID = " & rowID.ToString
            ''''                ConnectToSql(query, False)
            ''''                'MsgBox("deleted")
            ''''            End If
            ''''        End If
            ''''    Next i
            ''''End If



            ''''Call UpdateRowIDs()

            '''''query = "select c.ListID, c.LoanNo from DiallerStagingArea.dbo.CurrentAccounts c where c.RowID =0"
            ''''query = "select top 3 c.ListID, c.LoanNo from DiallerStagingArea.dbo.CurrentAccounts c where c.ListID = 191 and c.StagingAreaStatus =2"
            ''''loans = sqlToDataTable(query)
            ''''rows = loans.Rows.Count

            ''''un = "Int_Update"
            ''''pw = "Unc13Buck"

            ''''If rows > 0 Then
            ''''    MsgBox("Rows: " & rows.ToString)

            ''''    For Each row As DataRow In loans.Rows
            ''''        rowID = 0
            ''''        listID = row.Item(0)
            ''''        loanNo = row.Item(1)
            ''''        request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID.ToString & "' LoanNo='" & loanNo.ToString & "'/></data>"
            ''''        response = GetUltraDownload(request, un, pw)
            ''''        'MsgBox(response)
            ''''        rowID = GetValueFromXML(response, "RowId")

            ''''        If rowID <> 0 Then
            ''''            'MsgBox("ListID: " & listID.ToString & "; LoanNo: " & loanNo.ToString & "; RowID: " & rowID.ToString)

            ''''            query = "update c set RowID = " & rowID.ToString & " from DiallerStagingArea.dbo.CurrentAccounts c"
            ''''            query += " where c.ListID = " & listID.ToString & " and c.LoanNo = " & loanNo.ToString & " "
            ''''            MsgBox(query)
            ''''            Call ConnectToSql(query, False)
            ''''        Else
            ''''            MsgBox("ListID: " & listID.ToString & " & LoanNo: " & loanNo.ToString & " -> RowID is zero")
            ''''        End If
            ''''    Next row
            ''''Else
            ''''    MsgBox("No rows returned")
            ''''End If





            ''''listID = 191
            ''''rowID = 18591
            ''''request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID.ToString & "' RowId='" & rowID.ToString & "'/></data>"
            ''''response = GetUltraDownload(request, un, pw)
            ''''MsgBox(response)













            ''''Dim request As String = " < Data clientid='10864'><Report.DoNotCallList.Download DoNotCallList='1' /></data> "
            ''''MsgBox(request)
            ''''Dim un As String = "Int_Lists"
            ''''Dim pw As String = "Unc13Buck"
            ''''Dim response As String = "test"
            ''''MsgBox(response)
            ''''response = GetUltraDownload(request, un, pw)
            ''''MsgBox(response)

















            ''''Dim table As New DataTable
            ''''table = sqlToDataTable(query)
            ''''Dim cols As Integer = table.Columns.Count - 1
            ''''Dim rows As Integer = table.Rows.Count - 1
            ''''Dim data(rows, cols) As String
            ''''MsgBox("Got accounts to compare from db")

            ''''Do While rows > 0
            ''''    For i As Integer = 0 To rows
            ''''        With table
            ''''            loanNo = .Rows(i)(0)
            ''''            listID = .Rows(i)(1)
            ''''            rowID = .Rows(i)(2)
            ''''            batchID = .Rows(i)(3)
            ''''            'MsgBox("LoanNo: " & loanNo & "; ListID: " & listID & "; RowID: " & rowID & "; BatchID: " & batchID)
            ''''        End With

            ''''        schema = GetSchema(listID)
            ''''        request = "<data clientid='10864'><" & schema & " BaseIndexId='" & listID & "' RowId='" & rowID & "'/></data>"
            ''''        returnVal = UltraDownload(request)
            ''''        ultraState = GetValueFromXML(returnVal, "_State")
            ''''        claimID = GetValueFromXML(returnVal, "_ClaimId")
            ''''        schedule = GetValueFromXML(returnVal, "_Schedule")
            ''''        scheduled = GetValueFromXML(returnVal, "_Scheduled")
            ''''        schedulePriority = GetValueFromXML(returnVal, "_SchedulePriority")
            ''''        scheduleType = GetValueFromXML(returnVal, "_ScheduleType")

            ''''        query = "exec DiallerStagingArea.dbo.sp_AccountsToCompare_Return @loanNo = " & loanNo & " , @listID = " & listID & " , @rowID = " & rowID & " , @batchID = " & batchID _
            ''''        & " , @ultraState = " & ultraState & " , @claimID = '" & claimID.ToString & "' , @schedule = '" & schedule & "' , @scheduled = '" & scheduled.ToString _
            ''''        & "' , @schedulePriority = '" & schedulePriority.ToString & "' , @scheduleType = '" & scheduleType.ToString & "' "
            ''''        'MsgBox(query)
            ''''        Call ConnectToSql(query)
            ''''    Next i
            ''''    'MsgBox("Returned values from Ultra")

            ''''    Dim thisBox As TextBox = txtMonStart
            ''''    Dim startTime As String = GetNumberFromBox(thisBox.Text.ToString, 1).ToString & ":" & GetNumberFromBox(thisBox.Text.ToString, 0).ToString & ":00"
            ''''    startTime = Microsoft.VisualBasic.Left(startTime, 8)
            ''''    MsgBox(startTime)
            ''''    query = "exec DiallerStagingArea.dbo.sp_AccountsToCompare_Differ @batchID = " & batchID.ToString & ", @shiftStart = '" & startTime.ToString & "' "
            ''''    Call ConnectToSql(query, False)

            ''''    table = sqlToDataTable(query)
            ''''    cols = table.Columns.Count - 1
            ''''    rows = table.Rows.Count - 1
            ''''    ReDim data(rows, cols)
            ''''Loop





            MsgBox("Test complete")
            Else
                MsgBox("Password incorrect. Please try again.")
        End If
        cmbTest.Enabled = True
        'MsgBox("Nothing to see here at the moment!!")
    End Sub







    ''<<<<< Resources >>>>>''
    'http://howtostartprogramming.com/vb-net/
    'https://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqldatareader(v=vs.110).aspx
    'https://msdn.microsoft.com/en-us/library/system.data.sqlclient.sqldatareader.read(v=vs.110).aspx

    'HTTP Status Codes
    'http://www.restapitutorial.com/httpstatuscodes.html

    'Data reader to table
    'http://www.dotnetcurry.com/aspnet/143/convert-data-reader-to-data-table
    'https://www.dotnetperls.com/datatable-select-vbnet
    'https://www.dotnetperls.com/datarow-vbnet

    'Search datatable
    'http://stackoverflow.com/questions/4521919/creating-a-datatable-by-filtering-another-datatable
    'https://msdn.microsoft.com/en-us/library/way3dy9w(v=vs.110).aspx
    'https://msdn.microsoft.com/en-us/library/zk13kdh0(v=vs.90).aspx

    'DataViews
    'http://vb.net-informations.com/dataview/ado.net-dataview.htm

    'Dsitinct values from datatable
    'http://stackoverflow.com/questions/3535283/linq-vb-net-select-distinct-from-dataset
    'http://www.vbforums.com/showthread.php?628877-RESOLVED-Select-distinct-values-from-a-DataTable
    'http://stackoverflow.com/questions/19247504/retrieve-distinct-values-from-datatable-using-linq-vb-net

    'https://social.msdn.microsoft.com/Forums/vstudio/en-US/e6aad47f-1c61-448c-9cbe-fb427b680163/create-datatable-dynamically?forum=csharpgeneral
    'https://www.tutorialspoint.com/vb.net/vb.net_xml_processing.htm
    'https://msdn.microsoft.com/en-us/library/fs0z9zxd(v=vs.110).aspx
    'https://www.dotnetperls.com/datatable-vbnet
    'http://vb.net-informations.com/xml/read-xml-dataset-vb.net.htm
    'https://msdn.microsoft.com/en-us/library/system.data.datatable.writexml(v=vs.110).aspx
    'http://stackoverflow.com/questions/5239469/vb-net-creating-data-table
    'http://stackoverflow.com/questions/11240245/how-to-dynamically-create-columns-in-datatable-And-assign-values-to-it

    'Tread-safe calls to controls
    'https://msdn.microsoft.com/query/dev14.query?appId=Dev14IDEF1&l=EN-US&k=k(EHInvalidOperation.WinForms.IllegalCrossThreadCall);k(TargetFrameworkMoniker-.NETFramework,Version%3Dv4.5.2);k(DevLang-VB)&rd=true
    'http://stackoverflow.com/questions/23547990/how-to-thread-safe-calls-datagridview

    'Background operations
    'https://msdn.microsoft.com/en-us/library/hybbz6ke
    'https://www.dotnetperls.com/backgroundworker-vbnet
    'https://msdn.microsoft.com/en-us/library/system.componentmodel.backgroundworker(v=vs.110).aspx
    'http://www.java2s.com/Code/VB/GUI/BackgroundWorkerDemo.htm
    'http://stackoverflow.com/questions/7868623/how-to-run-function-in-background-in-vb-net

    'http://vb.net-informations.com/xml/create-xml-vb.net.htm
    'https://msdn.microsoft.com/en-us/library/c520bdhb.aspx
    'https://msdn.microsoft.com/en-us/library/1t4kyezf.aspx
    'https://msdn.microsoft.com/en-us/library/bb384609.aspx
    'http://vb.net-informations.com/xml/vb.net-xml-from-sql.htm
    'https://bytes.com/topic/visual-basic-net/answers/351458-retreiving-column-names-dataset-dataadapter
    'https://macunsw.wordpress.com/2010/11/19/vb-net-example-for-calling-web-service-by-posting-soap-xml/

    'Parse XML
    'https://msdn.microsoft.com/fr-fr/library/cc189056(v=vs.95).aspx
    'https://msdn.microsoft.com/fr-fr/library/cc189085(v=vs.95).aspx
    'https://www.codeproject.com/Articles/4826/XML-File-Parsing-in-VB-NET
    'https://www.tutorialspoint.com/vb.net/vb.net_xml_processing.htm
    'http://stackoverflow.com/questions/1656484/how-can-i-parse-xml-data-using-visual-basic
    'https://msdn.microsoft.com/en-us/library/ms766564(v=vs.85).aspx
    'https://msdn.microsoft.com/en-gb/library/aa468547.aspx
    'https://support.microsoft.com/en-us/help/317663/how-to-access-xml-data-using-dom-in-.net-framework-with-visual-basic-.net
    'https://www.codeproject.com/Articles/209954/Parsing-XML-file-in-VB-NET-using-DOM
    'https://www.simple-talk.com/sql/database-administration/manipulating-xml-data-in-sql-server/
End Class
