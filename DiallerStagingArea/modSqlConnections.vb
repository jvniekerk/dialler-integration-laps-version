﻿Option Explicit On
Imports System.Data.SqlClient

Module modSqlConnections
    Function ConnectSqlSendSOAPs(ByVal sqlQuery As String) As String
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim dt As New DataTable
        Dim listsTable As New DataTable
        Dim rowString As String = ""
        Dim thisColVal As String = ""
        Dim thisListID As String = ""
        Dim thisTable As DataTable
        Dim criteria() As String
        Dim thisListString As String = ""
        Dim i As Integer = 0
        Dim results As String = ""
        Dim sSchema As String = ""
        Dim sURL As String = "http://client.ultraasp.net/ultra.client.upload/service.asmx"
        Dim soapResponse As String = ""
        Dim batchID As String = "1"
        Dim query As String = ""
        Dim timeStamp As String = ""
        Dim thisRows As Integer
        Dim maxRows As Integer = 500
        Dim un As String = "UploaderSQL"
        Dim pw As String = "Password1"

        'force a minimum of 5 minutes between updates
        'MsgBox("starting")
        Dim start As DateTime = Now
        Dim minMins As Integer = 5 * 60

        'Try
        'Call Form1.updateListBox(Now.ToString & " -> Start Update", "lsbUpdateProgress")
        'MsgBox("about to send 1st query")
        query = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) Select 'Start Update' "
            Call ConnectToSql(query, False)
        'MsgBox("query ran")
        'MsgBox("stop now")

        'Call UpdateRowIDs()

        myConn = New SqlConnection(GetConnectionString())
            myCmd = myConn.CreateCommand
            myCmd.CommandTimeout = 0
            myCmd.CommandText = sqlQuery
            'Call Form1.updateListBox(Now.ToString & " -> Getting data from LAPS", "lsbUpdateProgress")

            myConn.Open()
            If myConn.State = ConnectionState.Open Then
                myReader = myCmd.ExecuteReader()
                dt.Load(myReader)
                myReader.Close()
                myReader = Nothing
                myConn.Close()
                myConn = Nothing

                'Call Form1.updateListBox(Now.ToString & " -> Got update data from LAPS", "lsbUpdateProgress")

                'MsgBox("Rows to upload: " & dt.Rows.Count.ToString)
                listsTable = dt.DefaultView.ToTable(True, "ListID")
                'MsgBox("Lists to upload to: " & listsTable.Rows.Count.ToString)

                ReDim criteria(4)
                criteria(0) = "NewUpload"
                criteria(1) = "Suppress"
                criteria(2) = "Deactivate"
                criteria(3) = "Reactivate"
                criteria(4) = "Alter"

                For Each row As DataRow In listsTable.Rows
                    thisListID = row.Item("ListID")
                    'MsgBox("thisListID: " & thisListID.ToString)

                    'Call Form1.updateListBox(Now.ToString & " -> updating list=" & thisListID, "lsbUpdateProgress")

                    For i = 0 To criteria.Length - 1
                        thisTable = FilterDataTable(dt, thisListID, criteria(i), criteria)
                        thisRows = thisTable.Rows.Count

                        'MsgBox("ListID: " & thisListID.ToString & " -> upload table rows: " & thisRows.ToString & " -> criteria: " & criteria(i))
                        If thisRows > 0 Then
                            sSchema = GetSchema(thisListID)

                            If Len(sSchema) > 0 Then
                                'Call Form1.updateListBox(Now.ToString & " -> updating list=" & thisListID & " criteria=" & criteria(i), "lsbUpdateProgress")

                                Dim uploadRows As Integer = maxRows
                                Dim smallTable As DataTable = thisTable.Clone

                                While thisTable.Rows.Count > 0
                                    If thisTable.Rows.Count < maxRows Then
                                        uploadRows = thisTable.Rows.Count
                                    Else
                                        uploadRows = maxRows
                                    End If

                                    For j As Integer = 0 To uploadRows - 1
                                        smallTable.ImportRow(thisTable.Rows(0))
                                        thisTable.Rows(0).Delete()
                                    Next j
                                    'MsgBox("ListID: " & thisListID.ToString & " -> upload table rows: " & smallTable.Rows.Count.ToString & " -> criteria: " & criteria(i))

                                    'thisListString = XMLfromDataTable(smallTable, criteria(i))
                                    'thisListString = buildEnvelope(thisListString, sSchema, thisListID)
                                    batchID = GetBatchID(thisListID, dt, criteria(i))


                                    'query = "select '- To Upload to Ultra -> BatchID: " & batchID.ToString & "; ListID: " & thisListID.ToString &
                                    '        "; Rows: " & uploadRows.ToString & "; Action: " & criteria(i).ToString & "' "
                                    'query = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) " & query
                                    'Call ConnectToSql(query, False)


                                    thisListString = BuildXML(sSchema, thisListID, smallTable, criteria(i))
                                    Dim BaseIndexID As String = "BaseIndexId='" & thisListID.ToString & "'"
                                    Dim NewBaseIndexID As String = Replace(BaseIndexID, "'", """")
                                    Dim ClientID As String = "clientid='10864'"
                                    Dim NewClientID As String = "clientid=""10864"""
                                    Dim NewListString = Replace(Replace(thisListString, BaseIndexID, NewBaseIndexID), ClientID, NewClientID)

                                    timeStamp = Now.ToString("yyyyMMdd_HHmmss")
                                    query = "exec DiallerStagingArea.dbo.sp_StoreUltraRequests @batchID = '" & batchID.ToString & "' , @listID = '" &
                                        thisListID.ToString & "' , @RequestXML = '" & NewListString & "' , @timeStamp ='" & timeStamp.ToString & "'"
                                    Call ConnectToSql(query, False)

                                    'Call Form1.updateListBox(Now.ToString & " -> store Ultra request", "lsbUpdateProgress")

                                    'soapResponse = sendSoap(sURL, thisListString, thisListID)
                                    soapResponse = GetUltraUpload(thisListString, un, pw)
                                    soapResponse = Replace(soapResponse, "rows count=", "rows batchID=""" & batchID.ToString & """ count=")
                                    'MsgBox("response: " & soapResponse)


                                    'MsgBox(thisListString)
                                    query = "exec DiallerStagingArea.dbo.sp_StoreUltraResponses_New @ResponseXML = '" & soapResponse & "' , @listID = " &
                                        thisListID.ToString & " , @batchID = " & batchID.ToString & " , @timeStamp ='" & timeStamp.ToString &
                                        "', @sentXML = '" & NewListString & "'"
                                    ConnectToSql(query, False)

                                    'Call Form1.updateListBox(Now.ToString & " -> Store Ultra response", "lsbUpdateProgress")


                                    query = "exec DiallerStagingArea.dbo.sp_StoreUltraResponseOutcomes @batchID = " & batchID.ToString &
                                        " , @listID = " & thisListID.ToString & " , @ResponseXML = '" & soapResponse &
                                        "', @sentXML = '" & NewListString & "'"
                                    ConnectToSql(query, False)

                                    'Call Form1.updateListBox(Now.ToString & " -> Store Ultra response outcomes", "lsbUpdateProgress")

                                    smallTable.Rows.Clear()
                                End While
                            End If
                        End If
                    Next i
                Next row
            End If

            Try
                Call UpdateRowIDs()
                Call DeleteOldAccounts()
            Catch
            End Try

            query = "insert into DiallerStagingArea.dbo.UpdateStoredProceduresRun ( ProcedureRun ) select 'Ultra Upload Complete' "
            Call ConnectToSql(query, False)

            'Call Form1.updateListBox(Now.ToString & " -> Update complete", "lsbUpdateProgress")
            'Catch
            'End Try


            'force a minimum of 5 minutes between updates
            Dim span As TimeSpan = Now - start
        Dim duration As Double = span.TotalSeconds
        Dim toWait As Double = (minMins - duration) * 1000
        'MsgBox("start: " & start & " -> now: " & Now & " -> duration: " & duration & " toWait: " & toWait)

        If toWait > 0 Then
            'MsgBox("waiting for " & toWait / 1000 & " seconds")
            'Call Form1.updateListBox(Now.ToString & " -> sleeping=" & toWait.ToString, "lsbUpdateProgress")
            Threading.Thread.Sleep(toWait)
        End If

        Return results
    End Function

    Function BuildXML(ByVal schema As String, ByVal listID As String, ByVal thisTable As DataTable, ByVal criteria As String) As String
        Dim xml As String = "<upload clientid='10864'><" & schema & " BaseIndexId='" & listID & "'><rows>"
        xml = xml & XMLfromDataTable(thisTable, criteria)
        xml = xml & "</rows></" & schema & "></upload>"
        'MsgBox(xml)

        Return xml
    End Function

    Function GetConnectionString() As String
        Dim hostname = System.Net.Dns.GetHostName()
        Dim ip As String = ""
        'Dim connString As String = "Initial Catalog=DiallerStagingArea;Data Source="

        For Each hostAdr In System.Net.Dns.GetHostEntry(hostname).AddressList()
            ip = hostAdr.ToString

            If ip = "172.27.63.23" Or ip = "172.27.63.17" Then
                Return "Initial Catalog=DiallerStagingArea;Data Source=UB-SQLCLSTR-1;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"
                '    connString += "UB-SQLCLSTR-1"
                'Else
                '    connString += "172.27.63.23"
            End If
        Next hostAdr

        'connString += ";User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=False;"

        Return "Initial Catalog=DiallerStagingArea;Data Source=172.27.63.23;User ID=SA;Password=u3zD2Ix6;Connect Timeout=0;Connection Timeout=0;Integrated Security=SSPI;"
        'Return connString
    End Function

    Public Function ConnectToSql(ByVal sqlQuery As String, Optional ByVal download As Boolean = True, Optional ByVal table As Boolean = False) As String
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader
        Dim results As String = ""

        'MsgBox(sqlQuery)

        myConn = New SqlConnection(GetConnectionString())
        myCmd = myConn.CreateCommand
        myCmd.CommandTimeout = 60 * 3
        myCmd.CommandText = sqlQuery
        myConn.Open()
        myReader = myCmd.ExecuteReader()

        If download Then
            results = XMLString(myReader)
        Else
            results = ReturnValue(myReader)
        End If

        myReader.Close()
        myReader = Nothing
        myConn.Close()
        myConn = Nothing

        Return results
    End Function

    Function sqlToDataTable(ByVal sqlQuery As String) As DataTable
        Dim myConn As SqlConnection
        Dim myCmd As SqlCommand
        Dim myReader As SqlDataReader

        myConn = New SqlConnection(GetConnectionString())
        myCmd = myConn.CreateCommand
        myCmd.CommandTimeout = 0
        myCmd.CommandText = sqlQuery
        myConn.Open()
        myReader = myCmd.ExecuteReader()

        Dim results As New DataTable
        results.Load(myReader)

        myReader.Close()
        myReader = Nothing
        myConn.Close()
        myConn = Nothing

        Return results
    End Function

    Function GetSchema(ByVal listID As String) As String
        If listID < "169" Then
            Select Case listID
                Case "155"
                    Return "NewIntegrationTest_13Feb17"
                Case "167"
                    Return "NewIntegrationTest_13Feb17"
                Case "168"
                    Return "NewIntegrationTest_13Feb17"
                Case Else
                    Return ""
            End Select
        Else
            Return "StagingAreaIntegration"
        End If
    End Function
End Module
